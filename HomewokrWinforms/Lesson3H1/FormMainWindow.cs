﻿using System;
using System.Windows.Forms;

namespace Lesson3H1
{
    public partial class FormMainWindow : Form
    {
        public FormMainWindow()
        {
            InitializeComponent();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Search_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog
            {
                ShowNewFolderButton = true
            };
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                FormSearchWindow s = new FormSearchWindow(folderBrowserDialog.SelectedPath);
                s.Show();
            }
        }
    }
}
