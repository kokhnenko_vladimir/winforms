﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Lesson3H1
{
    public partial class FormSearchWindow : Form
    {
        //private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public FormSearchWindow(string str)
        {
            InitializeComponent();

            labelInputPath.Text = str;
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public DialogResult ShowDialog(string s)
        {
            textBoxInputMask.Text = s;
            return ShowDialog();
        }

        public static List<string> MaskSearch(string dirName, string mask)
        {
            List<string> retVal = new List<string>();

            if (Directory.Exists(dirName))
            {
                try
                {
                    var files = Directory.GetFiles(dirName, mask);

                    foreach (var item in files)
                    {
                        retVal.Add(item);
                    }
                }
                catch (System.UnauthorizedAccessException) { }

                try
                {
                    var folders = Directory.GetDirectories(dirName);

                    foreach (var item in folders)
                        if ((File.GetAttributes(item)) == FileAttributes.Directory)
                        {
                            List<string> tmp = MaskSearch(item, mask);
                            foreach (var el in tmp)
                            {
                                retVal.Add(el);
                            }
                        }
                }
                catch (System.UnauthorizedAccessException) { }
            }

            return retVal;
        }

        private void TextBoxInputMask_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CodeReuseMethod();
            }
        }

        public void CodeReuseMethod()
        {
            Cursor.Current = Cursors.WaitCursor;
            listBoxFiles.Items.Clear();

            List<string> tmp = MaskSearch(labelInputPath.Text, textBoxInputMask.Text);

            foreach (string item in tmp)
            {
                listBoxFiles.Items.Add(item);
            }

            if (listBoxFiles.Items.Count == 0)
                listBoxFiles.Items.Add("Ничего не найдено");

            Cursor.Current = Cursors.Default;
        }

        private void ButtonEnter_Click(object sender, EventArgs e)
        {
            CodeReuseMethod();
        }

        private void ButtonDirrectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog
            {
                ShowNewFolderButton = false
            };

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                labelInputPath.Text = folderBrowserDialog.SelectedPath;
            }
        }
    }
}
