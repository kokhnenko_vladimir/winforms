﻿namespace Lesson3H1
{
    partial class FormSearchWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSearchWindow));
            this.textBoxInputMask = new System.Windows.Forms.TextBox();
            this.labelInputPath = new System.Windows.Forms.Label();
            this.listBoxFiles = new System.Windows.Forms.ListBox();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonDirrectory = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxInputMask
            // 
            this.textBoxInputMask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInputMask.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInputMask.Location = new System.Drawing.Point(16, 35);
            this.textBoxInputMask.Name = "textBoxInputMask";
            this.textBoxInputMask.Size = new System.Drawing.Size(1316, 13);
            this.textBoxInputMask.TabIndex = 1;
            this.textBoxInputMask.Text = "*.txt";
            this.textBoxInputMask.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInputMask.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxInputMask_KeyDown);
            // 
            // labelInputPath
            // 
            this.labelInputPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelInputPath.Location = new System.Drawing.Point(16, 9);
            this.labelInputPath.Name = "labelInputPath";
            this.labelInputPath.Size = new System.Drawing.Size(1316, 23);
            this.labelInputPath.TabIndex = 0;
            this.labelInputPath.Text = "Укажите папку для поиска";
            this.labelInputPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxFiles
            // 
            this.listBoxFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxFiles.FormattingEnabled = true;
            this.listBoxFiles.HorizontalScrollbar = true;
            this.listBoxFiles.Location = new System.Drawing.Point(16, 61);
            this.listBoxFiles.Name = "listBoxFiles";
            this.listBoxFiles.Size = new System.Drawing.Size(1376, 546);
            this.listBoxFiles.TabIndex = 4;
            // 
            // buttonEnter
            // 
            this.buttonEnter.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonEnter.Location = new System.Drawing.Point(626, 618);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(75, 23);
            this.buttonEnter.TabIndex = 5;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.ButtonEnter_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(707, 618);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 6;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.Close_Click);
            // 
            // buttonDirrectory
            // 
            this.buttonDirrectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDirrectory.Location = new System.Drawing.Point(1348, 20);
            this.buttonDirrectory.Name = "buttonDirrectory";
            this.buttonDirrectory.Size = new System.Drawing.Size(44, 22);
            this.buttonDirrectory.TabIndex = 7;
            this.buttonDirrectory.Text = "Path\r\n";
            this.buttonDirrectory.UseVisualStyleBackColor = true;
            this.buttonDirrectory.Click += new System.EventHandler(this.ButtonDirrectory_Click);
            // 
            // FormSearchWindow
            // 
            this.AcceptButton = this.buttonEnter;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(1408, 645);
            this.Controls.Add(this.buttonDirrectory);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.listBoxFiles);
            this.Controls.Add(this.labelInputPath);
            this.Controls.Add(this.textBoxInputMask);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(500, 250);
            this.Name = "FormSearchWindow";
            this.Text = "Поиск";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxInputMask;
        private System.Windows.Forms.Label labelInputPath;
        private System.Windows.Forms.ListBox listBoxFiles;
        private System.Windows.Forms.Button buttonEnter;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonDirrectory;
    }
}