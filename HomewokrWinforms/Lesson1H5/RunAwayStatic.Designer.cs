﻿namespace Lesson1H5
{
    partial class MainFormRunAwayStatic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRunawayStatic = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelRunawayStatic
            // 
            this.labelRunawayStatic.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelRunawayStatic.Location = new System.Drawing.Point(130, 163);
            this.labelRunawayStatic.MaximumSize = new System.Drawing.Size(100, 20);
            this.labelRunawayStatic.MinimumSize = new System.Drawing.Size(100, 20);
            this.labelRunawayStatic.Name = "labelRunawayStatic";
            this.labelRunawayStatic.Size = new System.Drawing.Size(100, 20);
            this.labelRunawayStatic.TabIndex = 0;
            this.labelRunawayStatic.Text = "RunawayStatic";
            this.labelRunawayStatic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelRunawayStatic.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RunawayStatic_MouseMove);
            // 
            // MainFormRunAwayStatic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelRunawayStatic);
            this.Name = "MainFormRunAwayStatic";
            this.Text = "Убегающий статик";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelRunawayStatic;
    }
}

