﻿using System.Drawing;
using System.Windows.Forms;

namespace Lesson1H5
{
    public partial class MainFormRunAwayStatic : Form
    {
        public MainFormRunAwayStatic()
        {
            InitializeComponent();
        }

        private void RunawayStatic_MouseMove(object sender, MouseEventArgs e)
        {
            const int step = 5;
            const int identWidth = 17;
            const int identHeight = 45;

            if (e.X < 20 && e.Y > 0 && e.Y < Size.Height)
            {
                if (labelRunawayStatic.Location.X + labelRunawayStatic.Size.Width < Size.Width - identWidth)
                    labelRunawayStatic.Location = new Point(labelRunawayStatic.Location.X + step, labelRunawayStatic.Location.Y);
                else
                    labelRunawayStatic.Location = new Point(0, labelRunawayStatic.Location.Y);
            }
            else if (e.X < labelRunawayStatic.Size.Width && e.X > labelRunawayStatic.Size.Width - identWidth && e.Y > 0 && e.Y < Size.Height)
            {
                if (labelRunawayStatic.Location.X != 0)
                    labelRunawayStatic.Location = new Point(labelRunawayStatic.Location.X - step, labelRunawayStatic.Location.Y);
                else
                    labelRunawayStatic.Location = new Point(Size.Width - identWidth - labelRunawayStatic.Size.Width, labelRunawayStatic.Location.Y);
            }
            else if (e.Y < 5 && e.X > 0 && e.X < Size.Width)
            {
                if (labelRunawayStatic.Location.Y + labelRunawayStatic.Size.Height < Size.Height - identHeight)
                    labelRunawayStatic.Location = new Point(labelRunawayStatic.Location.X, labelRunawayStatic.Location.Y + step);
                else
                    labelRunawayStatic.Location = new Point(labelRunawayStatic.Location.X, 0);
            }
            else if (e.Y < labelRunawayStatic.Size.Height && e.X > 0 && e.X < Size.Width)
            {
                if (labelRunawayStatic.Location.Y > 0)
                    labelRunawayStatic.Location = new Point(labelRunawayStatic.Location.X, labelRunawayStatic.Location.Y - step);
                else
                    labelRunawayStatic.Location = new Point(labelRunawayStatic.Location.X, Size.Height - 70);
            }
        }
    }
}
