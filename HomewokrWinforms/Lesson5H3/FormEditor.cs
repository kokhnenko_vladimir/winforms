﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Lesson5H3
{
    public partial class FormEditor : Form
    {
        private string _sourceString;
        private string _docName;

        public FormEditor()
        {
            InitializeComponent();
        }

        private void RichTextBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private async void RichTextBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            if (files[0].Length > 0)
            {
                if (Path.GetExtension(files[0]) == ".rtf")
                {
                    richTextBox.LoadFile(files[0]);
                    _sourceString = richTextBox.Text;
                    _docName = files[0];
                }
                else if (Path.GetExtension(files[0]) == ".txt")
                {
                    using (var sr = new StreamReader(files[0]))
                    {
                        richTextBox.Text = await sr.ReadToEndAsync();
                        _sourceString = richTextBox.Text;
                        _docName = files[0];
                    }
                }
            }
        }

        private async void ToolStripOpen_Click(object sender, EventArgs e)
        {
            try
            {
                int result = String.Compare(_sourceString, richTextBox.Text);
                if (!string.IsNullOrEmpty(_docName) && result != 0)
                {
                    if (MessageBox.Show(
                        "Save changes in file?", string.Empty, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        ToolStripSave_Click(this, EventArgs.Empty);
                    }
                }

                OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Filter = "Text Files (*.txt)|*.txt|RTF Files(*.rtf)|*.rtf",
                    FilterIndex = 1
                };

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog.FileName.Length != 0)
                    {
                        if (Path.GetExtension(openFileDialog.FileName) == ".rtf")
                        {
                            richTextBox.LoadFile(openFileDialog.FileName);
                            _sourceString = richTextBox.Text;
                            _docName = openFileDialog.FileName;
                        }
                        else
                        {
                            using (var sr = new StreamReader(openFileDialog.FileName))
                            {
                                richTextBox.Text = await sr.ReadToEndAsync();
                                _sourceString = richTextBox.Text;
                                _docName = openFileDialog.FileName;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private async void ToolStripSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(_docName))
                {
                    if (Path.GetExtension(_docName) == ".rtf")
                    {
                        richTextBox.SaveFile(_docName);
                        _sourceString = richTextBox.Text;
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(
                            _docName, false/*, System.Text.Encoding.Default*/))
                        {
                            await sw.WriteLineAsync(richTextBox.Text);
                            _sourceString = richTextBox.Text;
                        }
                    }
                }
                else
                {
                    SaveAsToolStripMenuItem_Click(this, EventArgs.Empty);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private async void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    Filter = "Text files (*.txt)|*.txt|RTF files (*.rtf)|*.rtf"
                };
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (Path.GetExtension(saveFileDialog.FileName) == ".rtf")
                    {
                        richTextBox.SaveFile(saveFileDialog.FileName);
                        _docName = saveFileDialog.FileName;
                        _sourceString = richTextBox.Text;
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(
                            saveFileDialog.FileName, false, System.Text.Encoding.Default))
                        {
                            await sw.WriteLineAsync(richTextBox.Text);
                            _docName = saveFileDialog.FileName;
                            _sourceString = richTextBox.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ToolStripFont_Click(object sender, EventArgs e)
        {
            FontDialog _fontDialog = new FontDialog
            {
                Font = richTextBox.SelectionFont
            };

            if (_fontDialog.ShowDialog() == DialogResult.OK)
                richTextBox.SelectionFont = _fontDialog.Font;
        }

        private void ToolStripColorFont_Click(object sender, EventArgs e)
        {
            ColorDialog _colorDialog = new ColorDialog
            {
                Color = richTextBox.SelectionColor,
                FullOpen = true
            };

            if (_colorDialog.ShowDialog() == DialogResult.OK)
                richTextBox.SelectionColor = _colorDialog.Color;
        }

        private void ToolStripColorBack_Click(object sender, EventArgs e)
        {
            ColorDialog _colorDialog = new ColorDialog
            {
                Color = richTextBox.SelectionColor,
                FullOpen = true
            };

            if (_colorDialog.ShowDialog() == DialogResult.OK)
                richTextBox.SelectionBackColor = _colorDialog.Color;
        }
    }
}