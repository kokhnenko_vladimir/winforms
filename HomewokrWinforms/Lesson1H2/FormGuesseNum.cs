﻿using System;
using System.Windows.Forms;

namespace Lesson1H22
{
    public partial class FormGuessNum : Form
    {
        public int InputNum { get; set; }

        public FormGuessNum()
        {
            InitializeComponent();
        }

        private void Game()
        {
            bool flag = false;
            int start = 0;
            int end = 1000;
            int tmp = end * 2;
            int count = 0;

            while (!flag)
            {
                string str = "Это число входит в диапазон?" + " от" + start.ToString() + " до " + end.ToString();
                DialogResult result = MessageBox.Show(str, "Ответьте пожалуйста", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Cancel)
                    Close();
                else if (result == DialogResult.Yes)
                {
                    if (end == start)
                    {
                        MessageBox.Show("Число отгадано за" + count.ToString() + "раз. Это " + end.ToString(), "Ответьте пожалуйста", MessageBoxButtons.OK);
                        flag = !flag;
                    }
                    else if (end - start == 4)
                    {
                        for (int i = start; i <= end; i++)
                        {
                            result = MessageBox.Show("Это число " + i.ToString() + " ?", "Ответьте пожалуйста", MessageBoxButtons.YesNoCancel);

                            if (result == DialogResult.Yes)
                            {
                                MessageBox.Show("Число отгадано за" + count.ToString() + "раз. Это " + i.ToString(), "Ответьте пожалуйста", MessageBoxButtons.OK);
                                flag = !flag;
                                break;
                            }

                            count++;
                        }
                    }

                    tmp = end;
                    end = (end - start) / 2 + start;
                    count++;
                }
                else if (result == DialogResult.No)
                {
                    start = end;
                    end = tmp;
                    count++;
                }
            }
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            Game();
        }
    }
}