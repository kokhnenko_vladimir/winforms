﻿namespace Lesson1H7
{
    partial class MainFormDayOfWeekAndTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnter = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelOutResultLabel = new System.Windows.Forms.Label();
            this.textBoxInputDate = new System.Windows.Forms.TextBox();
            this.radioButtonYear = new System.Windows.Forms.RadioButton();
            this.radioButtonMonths = new System.Windows.Forms.RadioButton();
            this.radioButtonDays = new System.Windows.Forms.RadioButton();
            this.radioButtonMinutes = new System.Windows.Forms.RadioButton();
            this.radioButtonSeconsds = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(65, 172);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(75, 23);
            this.buttonEnter.TabIndex = 6;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.EnterButton_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(220, 172);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 7;
            this.buttonClose.Text = "Exit";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // labelOutResultLabel
            // 
            this.labelOutResultLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelOutResultLabel.Location = new System.Drawing.Point(60, 73);
            this.labelOutResultLabel.Name = "labelOutResultLabel";
            this.labelOutResultLabel.Size = new System.Drawing.Size(250, 20);
            this.labelOutResultLabel.TabIndex = 8;
            this.labelOutResultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxInputDate
            // 
            this.textBoxInputDate.Location = new System.Drawing.Point(60, 35);
            this.textBoxInputDate.Name = "textBoxInputDate";
            this.textBoxInputDate.Size = new System.Drawing.Size(250, 20);
            this.textBoxInputDate.TabIndex = 0;
            this.textBoxInputDate.Text = "01.01.2020";
            this.textBoxInputDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radioButtonYear
            // 
            this.radioButtonYear.AutoSize = true;
            this.radioButtonYear.Location = new System.Drawing.Point(348, 38);
            this.radioButtonYear.Name = "radioButtonYear";
            this.radioButtonYear.Size = new System.Drawing.Size(43, 17);
            this.radioButtonYear.TabIndex = 1;
            this.radioButtonYear.Text = "Год";
            this.radioButtonYear.UseVisualStyleBackColor = true;
            this.radioButtonYear.Click += new System.EventHandler(this.RadioYear_Click);
            // 
            // radioButtonMonths
            // 
            this.radioButtonMonths.AutoSize = true;
            this.radioButtonMonths.Location = new System.Drawing.Point(348, 78);
            this.radioButtonMonths.Name = "radioButtonMonths";
            this.radioButtonMonths.Size = new System.Drawing.Size(58, 17);
            this.radioButtonMonths.TabIndex = 2;
            this.radioButtonMonths.Text = "Месяц";
            this.radioButtonMonths.UseVisualStyleBackColor = true;
            this.radioButtonMonths.Click += new System.EventHandler(this.RadioMonths_Click);
            // 
            // radioButtonDays
            // 
            this.radioButtonDays.AutoSize = true;
            this.radioButtonDays.Location = new System.Drawing.Point(348, 118);
            this.radioButtonDays.Name = "radioButtonDays";
            this.radioButtonDays.Size = new System.Drawing.Size(52, 17);
            this.radioButtonDays.TabIndex = 3;
            this.radioButtonDays.Text = "День";
            this.radioButtonDays.UseVisualStyleBackColor = true;
            this.radioButtonDays.Click += new System.EventHandler(this.RadioDays_Click);
            // 
            // radioButtonMinutes
            // 
            this.radioButtonMinutes.AutoSize = true;
            this.radioButtonMinutes.Location = new System.Drawing.Point(348, 158);
            this.radioButtonMinutes.Name = "radioButtonMinutes";
            this.radioButtonMinutes.Size = new System.Drawing.Size(62, 17);
            this.radioButtonMinutes.TabIndex = 4;
            this.radioButtonMinutes.Text = "Минута";
            this.radioButtonMinutes.UseVisualStyleBackColor = true;
            this.radioButtonMinutes.Click += new System.EventHandler(this.RadioMinutes_Click);
            // 
            // radioButtonSeconsds
            // 
            this.radioButtonSeconsds.AutoSize = true;
            this.radioButtonSeconsds.Location = new System.Drawing.Point(348, 198);
            this.radioButtonSeconsds.Name = "radioButtonSeconsds";
            this.radioButtonSeconsds.Size = new System.Drawing.Size(67, 17);
            this.radioButtonSeconsds.TabIndex = 5;
            this.radioButtonSeconsds.Text = "Секунда";
            this.radioButtonSeconsds.UseVisualStyleBackColor = true;
            this.radioButtonSeconsds.Click += new System.EventHandler(this.RadioSeconsds_Click);
            // 
            // MainFormDayOfWeekAndTime
            // 
            this.AcceptButton = this.buttonEnter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(482, 235);
            this.Controls.Add(this.radioButtonSeconsds);
            this.Controls.Add(this.radioButtonMinutes);
            this.Controls.Add(this.radioButtonDays);
            this.Controls.Add(this.radioButtonMonths);
            this.Controls.Add(this.radioButtonYear);
            this.Controls.Add(this.textBoxInputDate);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.labelOutResultLabel);
            this.Controls.Add(this.buttonEnter);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 250);
            this.Name = "MainFormDayOfWeekAndTime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "День недели и время";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonEnter;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelOutResultLabel;
        private System.Windows.Forms.TextBox textBoxInputDate;
        private System.Windows.Forms.RadioButton radioButtonYear;
        private System.Windows.Forms.RadioButton radioButtonMonths;
        private System.Windows.Forms.RadioButton radioButtonDays;
        private System.Windows.Forms.RadioButton radioButtonMinutes;
        private System.Windows.Forms.RadioButton radioButtonSeconsds;
    }
}

