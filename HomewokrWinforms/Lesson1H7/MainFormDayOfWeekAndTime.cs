﻿using System;
using System.Windows.Forms;

namespace Lesson1H7
{
    public partial class MainFormDayOfWeekAndTime : Form
    {
        public const int SecondsInYear = 31_536_000;
        public const int SecondsInMonts = 2_628_000;
        public const int SecondsInDays = 86_400;
        public const int MinutesInYear = 525_600;
        public const int MinutesInMonths = 43_800;
        public const int MinutesInDay = 1_440;

        public MainFormDayOfWeekAndTime()
        {
            InitializeComponent();
        }

        private void EnterButton_Click(object sender, EventArgs e)
        {
            DateTime d = GetInputDate();
            labelOutResultLabel.Text = $"День недели указанной даты - {d.DayOfWeek}";
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        public DateTime GetInputDate()
        {
            string str = string.Empty;
            int[] arrInt = new int[3];
            int count = 0;

            for (int i = 0; i < textBoxInputDate.Text.Length; i++)
            {
                if (textBoxInputDate.Text[i] >= '0' && textBoxInputDate.Text[i] <= '9')
                {
                    str += textBoxInputDate.Text[i].ToString();
                }
                else
                {
                    arrInt[count++] = Convert.ToInt32(str);
                    str = string.Empty;
                }
            }
            arrInt[count] = Convert.ToInt32(str);

            return new DateTime(arrInt[2], arrInt[1], arrInt[0]);
        }

        private void RadioYear_Click(object sender, EventArgs e)
        {
            labelOutResultLabel.Text = $"{(GetInputDate() - DateTime.Now).Days} / 365";
        }

        private void RadioMonths_Click(object sender, EventArgs e)
        {
            labelOutResultLabel.Text = $"{(GetInputDate() - DateTime.Now).Days} / 31";
        }

        private void RadioDays_Click(object sender, EventArgs e)
        {
            labelOutResultLabel.Text = (GetInputDate() - DateTime.Now).Days.ToString();
        }

        private void RadioMinutes_Click(object sender, EventArgs e)
        {
            DateTime d1 = DateTime.Now;
            DateTime d2 = GetInputDate();

            labelOutResultLabel.Text = ((d2 - d1).Days * MinutesInDay + (d2 - d1).Hours * 60 + (d2 - d1).Minutes).ToString();
        }

        private void RadioSeconsds_Click(object sender, EventArgs e)
        {
            DateTime d1 = DateTime.Now;
            DateTime d2 = GetInputDate();

            labelOutResultLabel.Text = ((d2 - d1).Days * SecondsInDays + (d2 - d1).Hours * 3600 + (d2 - d1).Minutes * 60 + (d2 - d1).Seconds).ToString();
        }
    }
}
