﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Lesson1H1
{
    public partial class FormMyResume : Form
    {
        private readonly List<string> _str;

        public FormMyResume()
        {
            InitializeComponent();

            _str = new List<string>
            {
                "Цель резюме: соискание должности программиста С++",
                "Дата рождения: 01 июля 1988",
                "Адрес: с.Крыжановка, ул.Бескоровайнова д.31",
                "Телефон:+38(063) 5975591",
                "Семейное положение: не женат.",
                "Образование высшее и учусь:",
                "Компьютерная Акадамия Шаг, 11/2018-наст.вр., студент (направление IT),",
                "\"Одесская Государственная Академия Холода\", 2005-2010 г.г., специальность: инженер-энергетик (специалист) холодильные машины и установки.",
                "Опыт работы:",
                "10/2014-наст.вр., частный предприниматель - оказываю услуги по экспертизе автозапчастей, 08/2010-08/2014, ДП \"ГСИ-УКРНЕФТЕГАЗСТРОЙ\" - мастер по ремонту запорной арматуры.",
                "Дополнительная информация:",
                "языки: русский, украинский,",
                "владение компьютером на уровне опытного пользователя (Word, Exel),",
                "личные качества: математический склад ума, умение анализировать."
            };

        }

        private void Resume_Click(object sender, EventArgs e)
        {
            int countMes = 0;
            int sum = 0;

            for (int i = 0; i < _str.Count; i++)
            {
                if (i != _str.Count - 1)
                    MessageBox.Show(
                        _str[i],
                        "Резюме Кохненко Владимира Юрьевича",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                sum += _str[i].Length;
                countMes++;
            }

            MessageBox.Show(
                        _str[_str.Count - 1],
                        $"Среднее число символов: {sum / countMes}",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
        }
    }
}
