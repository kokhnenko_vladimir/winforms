﻿namespace Lesson7H2
{
    partial class FormDiagram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textValue0 = new System.Windows.Forms.TextBox();
            this.value1 = new System.Windows.Forms.Label();
            this.value2 = new System.Windows.Forms.Label();
            this.textValue1 = new System.Windows.Forms.TextBox();
            this.value3 = new System.Windows.Forms.Label();
            this.textValue2 = new System.Windows.Forms.TextBox();
            this.value4 = new System.Windows.Forms.Label();
            this.textValue3 = new System.Windows.Forms.TextBox();
            this.value5 = new System.Windows.Forms.Label();
            this.textValue4 = new System.Windows.Forms.TextBox();
            this.value6 = new System.Windows.Forms.Label();
            this.textValue5 = new System.Windows.Forms.TextBox();
            this.value7 = new System.Windows.Forms.Label();
            this.textValue6 = new System.Windows.Forms.TextBox();
            this.value8 = new System.Windows.Forms.Label();
            this.textValue7 = new System.Windows.Forms.TextBox();
            this.value9 = new System.Windows.Forms.Label();
            this.textValue8 = new System.Windows.Forms.TextBox();
            this.value10 = new System.Windows.Forms.Label();
            this.textValue9 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textValue0
            // 
            this.textValue0.Location = new System.Drawing.Point(5, 744);
            this.textValue0.Name = "textValue0";
            this.textValue0.Size = new System.Drawing.Size(61, 20);
            this.textValue0.TabIndex = 0;
            this.textValue0.Text = "0";
            this.textValue0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue0_KeyDown);
            // 
            // value1
            // 
            this.value1.AutoSize = true;
            this.value1.Location = new System.Drawing.Point(12, 728);
            this.value1.Name = "value1";
            this.value1.Size = new System.Drawing.Size(43, 13);
            this.value1.TabIndex = 1;
            this.value1.Text = "Value 1";
            // 
            // value2
            // 
            this.value2.AutoSize = true;
            this.value2.Location = new System.Drawing.Point(116, 728);
            this.value2.Name = "value2";
            this.value2.Size = new System.Drawing.Size(43, 13);
            this.value2.TabIndex = 3;
            this.value2.Text = "Value 2";
            // 
            // textValue1
            // 
            this.textValue1.Location = new System.Drawing.Point(109, 744);
            this.textValue1.Name = "textValue1";
            this.textValue1.Size = new System.Drawing.Size(61, 20);
            this.textValue1.TabIndex = 2;
            this.textValue1.Text = "0";
            this.textValue1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue1_KeyDown);
            // 
            // value3
            // 
            this.value3.AutoSize = true;
            this.value3.Location = new System.Drawing.Point(220, 728);
            this.value3.Name = "value3";
            this.value3.Size = new System.Drawing.Size(43, 13);
            this.value3.TabIndex = 5;
            this.value3.Text = "Value 3";
            // 
            // textValue2
            // 
            this.textValue2.Location = new System.Drawing.Point(213, 744);
            this.textValue2.Name = "textValue2";
            this.textValue2.Size = new System.Drawing.Size(61, 20);
            this.textValue2.TabIndex = 4;
            this.textValue2.Text = "0";
            this.textValue2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue2_KeyDown);
            // 
            // value4
            // 
            this.value4.AutoSize = true;
            this.value4.Location = new System.Drawing.Point(324, 728);
            this.value4.Name = "value4";
            this.value4.Size = new System.Drawing.Size(43, 13);
            this.value4.TabIndex = 7;
            this.value4.Text = "Value 4";
            // 
            // textValue3
            // 
            this.textValue3.Location = new System.Drawing.Point(317, 744);
            this.textValue3.Name = "textValue3";
            this.textValue3.Size = new System.Drawing.Size(61, 20);
            this.textValue3.TabIndex = 6;
            this.textValue3.Text = "0";
            this.textValue3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue3_KeyDown);
            // 
            // value5
            // 
            this.value5.AutoSize = true;
            this.value5.Location = new System.Drawing.Point(428, 728);
            this.value5.Name = "value5";
            this.value5.Size = new System.Drawing.Size(43, 13);
            this.value5.TabIndex = 9;
            this.value5.Text = "Value 5";
            // 
            // textValue4
            // 
            this.textValue4.Location = new System.Drawing.Point(421, 744);
            this.textValue4.Name = "textValue4";
            this.textValue4.Size = new System.Drawing.Size(61, 20);
            this.textValue4.TabIndex = 8;
            this.textValue4.Text = "0";
            this.textValue4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue4_KeyDown);
            // 
            // value6
            // 
            this.value6.AutoSize = true;
            this.value6.Location = new System.Drawing.Point(532, 728);
            this.value6.Name = "value6";
            this.value6.Size = new System.Drawing.Size(43, 13);
            this.value6.TabIndex = 11;
            this.value6.Text = "Value 6";
            // 
            // textValue5
            // 
            this.textValue5.Location = new System.Drawing.Point(525, 744);
            this.textValue5.Name = "textValue5";
            this.textValue5.Size = new System.Drawing.Size(61, 20);
            this.textValue5.TabIndex = 10;
            this.textValue5.Text = "0";
            this.textValue5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue5_KeyDown);
            // 
            // value7
            // 
            this.value7.AutoSize = true;
            this.value7.Location = new System.Drawing.Point(636, 728);
            this.value7.Name = "value7";
            this.value7.Size = new System.Drawing.Size(43, 13);
            this.value7.TabIndex = 13;
            this.value7.Text = "Value 7";
            // 
            // textValue6
            // 
            this.textValue6.Location = new System.Drawing.Point(629, 744);
            this.textValue6.Name = "textValue6";
            this.textValue6.Size = new System.Drawing.Size(61, 20);
            this.textValue6.TabIndex = 12;
            this.textValue6.Text = "0";
            this.textValue6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue6_KeyDown);
            // 
            // value8
            // 
            this.value8.AutoSize = true;
            this.value8.Location = new System.Drawing.Point(740, 728);
            this.value8.Name = "value8";
            this.value8.Size = new System.Drawing.Size(43, 13);
            this.value8.TabIndex = 15;
            this.value8.Text = "Value 8";
            // 
            // textValue7
            // 
            this.textValue7.Location = new System.Drawing.Point(733, 744);
            this.textValue7.Name = "textValue7";
            this.textValue7.Size = new System.Drawing.Size(61, 20);
            this.textValue7.TabIndex = 14;
            this.textValue7.Text = "0";
            this.textValue7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue7_KeyDown);
            // 
            // value9
            // 
            this.value9.AutoSize = true;
            this.value9.Location = new System.Drawing.Point(844, 728);
            this.value9.Name = "value9";
            this.value9.Size = new System.Drawing.Size(43, 13);
            this.value9.TabIndex = 17;
            this.value9.Text = "Value 9";
            // 
            // textValue8
            // 
            this.textValue8.Location = new System.Drawing.Point(837, 744);
            this.textValue8.Name = "textValue8";
            this.textValue8.Size = new System.Drawing.Size(61, 20);
            this.textValue8.TabIndex = 16;
            this.textValue8.Text = "0";
            this.textValue8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue8_KeyDown);
            // 
            // value10
            // 
            this.value10.AutoSize = true;
            this.value10.Location = new System.Drawing.Point(948, 728);
            this.value10.Name = "value10";
            this.value10.Size = new System.Drawing.Size(49, 13);
            this.value10.TabIndex = 19;
            this.value10.Text = "Value 10";
            // 
            // textValue9
            // 
            this.textValue9.Location = new System.Drawing.Point(943, 744);
            this.textValue9.Name = "textValue9";
            this.textValue9.Size = new System.Drawing.Size(61, 20);
            this.textValue9.TabIndex = 18;
            this.textValue9.Text = "0";
            this.textValue9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextValue9_KeyDown);
            // 
            // FormDiagram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1128, 771);
            this.Controls.Add(this.value10);
            this.Controls.Add(this.textValue9);
            this.Controls.Add(this.value9);
            this.Controls.Add(this.textValue8);
            this.Controls.Add(this.value8);
            this.Controls.Add(this.textValue7);
            this.Controls.Add(this.value7);
            this.Controls.Add(this.textValue6);
            this.Controls.Add(this.value6);
            this.Controls.Add(this.textValue5);
            this.Controls.Add(this.value5);
            this.Controls.Add(this.textValue4);
            this.Controls.Add(this.value4);
            this.Controls.Add(this.textValue3);
            this.Controls.Add(this.value3);
            this.Controls.Add(this.textValue2);
            this.Controls.Add(this.value2);
            this.Controls.Add(this.textValue1);
            this.Controls.Add(this.value1);
            this.Controls.Add(this.textValue0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormDiagram";
            this.Text = "Diagram";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormDiagram_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textValue0;
        private System.Windows.Forms.Label value1;
        private System.Windows.Forms.Label value2;
        private System.Windows.Forms.TextBox textValue1;
        private System.Windows.Forms.Label value3;
        private System.Windows.Forms.TextBox textValue2;
        private System.Windows.Forms.Label value4;
        private System.Windows.Forms.TextBox textValue3;
        private System.Windows.Forms.Label value5;
        private System.Windows.Forms.TextBox textValue4;
        private System.Windows.Forms.Label value6;
        private System.Windows.Forms.TextBox textValue5;
        private System.Windows.Forms.Label value7;
        private System.Windows.Forms.TextBox textValue6;
        private System.Windows.Forms.Label value8;
        private System.Windows.Forms.TextBox textValue7;
        private System.Windows.Forms.Label value9;
        private System.Windows.Forms.TextBox textValue8;
        private System.Windows.Forms.Label value10;
        private System.Windows.Forms.TextBox textValue9;
    }
}

