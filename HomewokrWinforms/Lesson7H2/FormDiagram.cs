﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lesson7H2
{
    public partial class FormDiagram : Form
    {
        private const int _bottomForCoordinate = 700;
        private const int _leftForCoordinate = 30;
        private const int _right = 20;
        private const int _leftForMarkup = 10;

        private readonly Brush[] _brushes = new Brush[]
            {
                Brushes.Black, Brushes.LightGray, Brushes.AliceBlue,
                Brushes.Aqua, Brushes.Aquamarine, Brushes.Beige,
                Brushes.Bisque, Brushes.BurlyWood, Brushes.Coral,
                Brushes.Cyan
            };

        private readonly int[] _valuesForRectangles = new int[10];

        public FormDiagram()
        {
            InitializeComponent();
        }

        private void FormDiagram_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            g.DrawLine(
                new Pen(Brushes.Blue, 2f),
                new Point(_leftForCoordinate, _bottomForCoordinate),
                new Point(Size.Width - _right, _bottomForCoordinate));
            g.DrawLine(
                new Pen(Brushes.Blue, 2f),
                new Point(_leftForCoordinate, 0),
                new Point(_leftForCoordinate, _bottomForCoordinate));

            for (int i = 20; i < _bottomForCoordinate; i += 20)
            {
                g.DrawLine(
                    new Pen(Brushes.Green, 0.5f),
                    new Point(_leftForMarkup, i),
                    new Point(Size.Width - _right, i));
            }

            int start = _leftForCoordinate + 100;
            int end = _leftForCoordinate + 1100;

            for (int i = start, j = 0; i < end; i += 100, j++)
            {

                g.DrawLine(
                    new Pen(_brushes[j], 100f),
                    new Point(i, _bottomForCoordinate),
                    new Point(i,
                    _valuesForRectangles[j] > 0 ? _bottomForCoordinate - _valuesForRectangles[j] : _bottomForCoordinate));
            }
        }

        private void TextValue0_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue0, 0);
        }

        private void TextValue1_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue1, 1);
        }

        private void TextValue2_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue2, 2);
        }

        private void TextValue3_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue3, 3);
        }

        private void TextValue4_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue4, 4);
        }

        private void TextValue5_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue5, 5);
        }

        private void TextValue6_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue6, 6);
        }

        private void TextValue7_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue7, 7);
        }

        private void TextValue8_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue8, 8);
        }

        private void TextValue9_KeyDown(object sender, KeyEventArgs e)
        {
            Work(e, textValue9, 9);
        }
        private void Work(KeyEventArgs e, TextBox textBox, int index)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    int num;
                    if ((num = Convert.ToInt32(textBox.Text)) >= 0 &&
                        (num = Convert.ToInt32(textBox.Text)) <= 700)
                    {
                        _valuesForRectangles[index] = num;
                        Invalidate();
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    textBox.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}