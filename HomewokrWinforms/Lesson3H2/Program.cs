﻿using Lesson3H2.Model;
using Lesson3H2.Presenters;
using Lesson3H2.Views;
using System;
using System.Windows.Forms;

namespace Lesson3H2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var mainPres = new PresenterFormComponentEditing(new FormComponentEditing(), new ModelData());

            Application.Run((Form)mainPres.View);
        }
    }
}