﻿using System;

namespace Lesson3H2.Model
{
    [Serializable]
    class ComputerComponents
    {
        private string _name;
        private string _characteristics;
        private decimal _price;

        public ComputerComponents(string name, string characteristics, decimal price)
        {
            _name = name;
            _characteristics = characteristics;
            _price = price;
        }

        public string Name { get => _name; set => _name = value; }
        public string Characteristic { get => _characteristics; set => _characteristics = value; }
        public decimal Price { get => _price; set => _price = value; }
    }
}