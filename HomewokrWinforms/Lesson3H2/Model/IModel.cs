﻿using System.Collections.Generic;

namespace Lesson3H2.Model
{
    interface IModel
    {
        List<ComputerComponents> Data { get; set; }

        void SaveInFile();
        ModelData LoadFromFile();
    }
}