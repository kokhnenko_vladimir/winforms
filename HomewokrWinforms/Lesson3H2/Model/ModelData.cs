﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Lesson3H2.Model
{
    [Serializable]
    class ModelData : IModel
    {
        private List<ComputerComponents> _data = new List<ComputerComponents>();

        public List<ComputerComponents> Data { get => _data; set => _data = value; }

        public void SaveInFile()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("ModelData.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
            }
        }

        public ModelData LoadFromFile()
        {
            BinaryFormatter formatter = new BinaryFormatter();

            ModelData newModel;

            using (FileStream fs = new FileStream("ModelData.dat", FileMode.OpenOrCreate))
            {
                if (fs.Length == 0)
                    newModel = new ModelData();
                else
                    newModel = (ModelData)formatter.Deserialize(fs);
            }
            return newModel;
        }
    }
}