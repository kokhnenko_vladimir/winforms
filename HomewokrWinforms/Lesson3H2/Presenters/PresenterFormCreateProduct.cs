﻿using Lesson3H2.Model;
using Lesson3H2.Views;
using System;
using System.Windows.Forms;

namespace Lesson3H2.Presenters
{
    class PresenterFormCreateProduct
    {
        private readonly IModel _model;
        private readonly IViewFormCreateProduct _view;

        public PresenterFormCreateProduct(IViewFormCreateProduct view, IModel model)
        {
            _model = model;
            _view = view;

            _view.ButtonAddProductEvent += ButtonAddProductClick;
            _view.TextBoxNameChangeTextEvent += TextBoxNameTextChanged;
            _view.TextBoxCharacteristicsChangeTextEvent += TextBoxCharacteristicsTextChanged;
            _view.TextBoxPriceChangeTextEvent += TextBoxPriceTextChanged;

            _view.ButtonAddProduct.Enabled = false;
        }

        #region Private

        private void TextBoxNameTextChanged(object sender, EventArgs e)
        {
            EnableButtonAddProduct();
        }

        private void TextBoxCharacteristicsTextChanged(object sender, EventArgs e)
        {
            EnableButtonAddProduct();
        }

        private void TextBoxPriceTextChanged(object sender, EventArgs e)
        {
            if (_view.TextPrice.Text.Length > 0)
            {
                bool _isThatRight = true;

                foreach (var ch in _view.TextPrice.Text)
                {
                    if (!char.IsDigit(ch) && ch != ',')
                    {
                        MessageBox.Show("Некорректный ввод");
                        _isThatRight = false;
                        _view.TextPrice.Text = string.Empty;
                        break;
                    }
                }

                try
                {
                    if (_isThatRight && _view.TextPrice.Text[_view.TextPrice.Text.Length - 1] != ',')
                    {
                        Convert.ToDecimal(_view.TextPrice.Text);

                        EnableButtonAddProduct();
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Некорректный ввод");
                    _view.TextPrice.Text = string.Empty;
                }
                catch (OverflowException)
                {
                    MessageBox.Show("Некорректный ввод");
                    _view.TextPrice.Text = string.Empty;
                }
            }
        }

        private void ButtonAddProductClick(object sender, EventArgs e)
        {
            _model.Data.Add(new ComputerComponents(
                _view.TextBoxName.Text, _view.TextBoxCharacteristics.Text, Convert.ToDecimal(_view.TextPrice.Text)));

            _view.TextBoxName.Text = string.Empty;
            _view.TextBoxCharacteristics.Text = string.Empty;
            _view.TextPrice.Text = string.Empty;
        }

        private bool AreTheFieldsFilledIn()
        {
            if (_view.TextBoxName.Text.Length > 0 && _view.TextBoxCharacteristics.Text.Length > 0 &&
                _view.TextPrice.Text.Length > 0)
                return true;
            return false;
        }

        private void EnableButtonAddProduct()
        {
            if (AreTheFieldsFilledIn())
                _view.ButtonAddProduct.Enabled = true;
            else
                _view.ButtonAddProduct.Enabled = false;
        }

        #endregion
    }
}
