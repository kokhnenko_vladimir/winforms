﻿using Lesson3H2.Model;
using Lesson3H2.Views;
using System;
using System.Windows.Forms;

namespace Lesson3H2.Presenters
{
    class PresenterFormSalesRecord
    {
        private readonly IViewFormSalesRecord _view;
        private readonly IModel _model;
        private readonly int _index;

        public PresenterFormSalesRecord(IViewFormSalesRecord view, IModel model, int index)
        {
            _view = view;
            _model = model;
            _index = index;

            _view.ButtonSaveEvent += ButtonSaveClick;
            _view.ButtonUndoEvent += ButtonUndoClick;
            _view.FormRedactorComponentsLoadEvent += FormRedactorComponentsLoad;
            _view.TextBoxPriceTextChangedEvent += TextBoxPriceTextChanged;
        }
        private void FormRedactorComponentsLoad(object sender, EventArgs e)
        {
            _view.TextBoxRedactorNameProp.Text = _model.Data[_index].Name;
            _view.TextBoxRedactorCharacteristicsProp.Text = _model.Data[_index].Characteristic;
            _view.TextBoxPriceProp.Text = _model.Data[_index].Price.ToString();
        }

        private void ButtonSaveClick(object Sender, EventArgs e)
        {
            _model.Data[_index].Name = _view.TextBoxRedactorNameProp.Text;
            _model.Data[_index].Characteristic = _view.TextBoxRedactorCharacteristicsProp.Text;
            _model.Data[_index].Price = Convert.ToDecimal(_view.TextBoxPriceProp.Text);
        }

        private void ButtonUndoClick(object Sender, EventArgs e)
        {
            ((Form)_view).Close();
        }

        private void TextBoxPriceTextChanged(object Sender, EventArgs e)
        {
            if (_view.TextBoxPriceProp.Text.Length > 0)
            {
                bool _isThatRight = true;

                foreach (var ch in _view.TextBoxPriceProp.Text)
                {
                    if (!char.IsDigit(ch) && ch != ',')
                    {
                        MessageBox.Show("Некорректный ввод");
                        _isThatRight = false;
                        _view.TextBoxPriceProp.Text = string.Empty;
                        break;
                    }
                }

                try
                {
                    if (_isThatRight && _view.TextBoxPriceProp.Text[_view.TextBoxPriceProp.Text.Length - 1] != ',')
                    {
                        Convert.ToDecimal(_view.TextBoxPriceProp.Text);
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Некорректный ввод");
                    _view.TextBoxPriceProp.Text = string.Empty;
                }
                catch (OverflowException)
                {
                    MessageBox.Show("Некорректный ввод");
                    _view.TextBoxPriceProp.Text = string.Empty;
                }
            }
        }
    }
}