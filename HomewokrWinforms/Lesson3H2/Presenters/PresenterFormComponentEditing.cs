﻿using Lesson3H2.Model;
using Lesson3H2.Views;
using System;
using System.Threading.Tasks;

namespace Lesson3H2.Presenters
{
    class PresenterFormComponentEditing
    {
        private readonly IViewFormComponentEditing _view;
        private IModel _model;

        public IViewFormComponentEditing View { get => _view; }

        public PresenterFormComponentEditing(IViewFormComponentEditing view, IModel model)
        {
            _view = view;

            Task.Factory.StartNew(() => { _model = model.LoadFromFile(); });

            _view.NameOfProductSelectedIndexEvent += NameOfProductSelectedIndexEvent;
            _view.ButtonAddClickEvent += ButtonAddClickEvent;
            _view.ButtonDeleteEvent += ButtonDeleteEvent;
            _view.ButtonRedactorEvent += ButtonRedactorEvent;
            _view.ButtonCreateProductEvent += ButtonCreateProductEvent;
            _view.ListViewComponentsMouseClickEvent += ListViewComponentsMouseClickEvent;
            _view.ButtonSaveDataEvent += ButtonSaveDataClickEvent;
            _view.FormComponentEditingLoadEvent += FormComponentEditingLoad;

        }

        #region Public

        public void LoadModel()
        {
            _model.LoadFromFile();
        }

        public void SaveModel()
        {
            _model.SaveInFile();
        }

        #endregion

        #region Private

        public bool IsEmptyComboBoxNameOfProducts
        {
            get => _view.ComboBoxNameOfProducts.Items.Count != 0;
        }

        private void NameOfProductSelectedIndexEvent(object sender, EventArgs e)
        {
            if (IsEmptyComboBoxNameOfProducts)
            {
                _view.TextBoxUnitCost.Text = _model.Data[_view.ComboBoxNameOfProducts.SelectedIndex].Price.ToString();
                _view.ButtonRedactor.Enabled = true;
                _view.ButtonAdd.Enabled = true;
            }
        }

        private void ButtonAddClickEvent(object sender, EventArgs e)
        {
            if (IsEmptyComboBoxNameOfProducts && _view.ComboBoxNameOfProducts.SelectedIndex >= 0 &&
                _view.ComboBoxNameOfProducts.SelectedIndex < _view.ComboBoxNameOfProducts.Items.Count)
            {
                _view.ListViewComponents.Items.Add(_model.Data[_view.ComboBoxNameOfProducts.SelectedIndex].Name);
                _view.ListViewComponents.Items[_view.ListViewComponents.Items.Count - 1].SubItems.Add(
                    _model.Data[_view.ComboBoxNameOfProducts.SelectedIndex].Characteristic);
                _view.ListViewComponents.Items[_view.ListViewComponents.Items.Count - 1].SubItems.Add(
                    _model.Data[_view.ComboBoxNameOfProducts.SelectedIndex].Price.ToString());

                decimal sum = 0;

                for (int i = 0; i < _view.ListViewComponents.Items.Count; i++)
                {
                    sum += Convert.ToDecimal(_view.ListViewComponents.Items[i].SubItems[2].Text);
                }

                _view.TextBoxTotalCost.Text = sum.ToString();
            }
        }

        private void ButtonDeleteEvent(object sender, EventArgs e)
        {
            if (_view.ListViewComponents.SelectedIndices.Count != 0)
            {
                _view.ListViewComponents.Items.RemoveAt(_view.ListViewComponents.SelectedIndices[0]);

                decimal sum = 0;

                for (int i = 0; i < _view.ListViewComponents.Items.Count; i++)
                {
                    sum += Convert.ToDecimal(_view.ListViewComponents.Items[i].SubItems[2].Text);
                }

                _view.TextBoxTotalCost.Text = sum.ToString();
                _view.ButtonDelete.Enabled = false;
            }
        }

        private void ButtonRedactorEvent(object sender, EventArgs e)
        {
            var formRecord = new FormRedactorComponents();
            new PresenterFormSalesRecord(formRecord, _model, _view.ComboBoxNameOfProducts.SelectedIndex);

            formRecord.ShowDialog();
        }

        private void ButtonCreateProductEvent(object sender, EventArgs e)
        {
            var formCreateProduct = new FormCreateProduct();
            new PresenterFormCreateProduct(formCreateProduct, _model);

            formCreateProduct.ShowDialog();
            _view.ComboBoxNameOfProducts.Items.Clear();

            foreach (var item in _model.Data)
            {
                _view.ComboBoxNameOfProducts.Items.Add(item.Name);
            }
        }

        private void ListViewComponentsMouseClickEvent(object sender, EventArgs e)
        {
            _view.ButtonDelete.Enabled = true;
        }

        private void ButtonSaveDataClickEvent(object sender, EventArgs e)
        {
            _model.SaveInFile();
        }

        private void FormComponentEditingLoad(object sender, EventArgs e)
        {
            for (int i = 0; i < _model.Data.Count; i++)
                _view.ComboBoxNameOfProducts.Items.Add(_model.Data[i].Name);
            if (_view.ComboBoxNameOfProducts.Items.Count != 0)
            {
                _view.ComboBoxNameOfProducts.SelectedIndex = 0;
                _view.TextBoxUnitCost.Text = _model.Data[0].Price.ToString();
            }
        }

        #endregion
    }
}