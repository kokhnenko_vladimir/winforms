﻿using System;
using System.Windows.Forms;

namespace Lesson3H2.Views
{
    public partial class FormComponentEditing : Form, IViewFormComponentEditing
    {
        public FormComponentEditing()
        {
            InitializeComponent();
        }

        ComboBox IViewFormComponentEditing.ComboBoxNameOfProducts { get => ComboBoxNameOfProducts; }
        TextBox IViewFormComponentEditing.TextBoxUnitCost { get => TextBoxUnitCost; }
        TextBox IViewFormComponentEditing.TextBoxTotalCost { get => TextBoxTotalCost; }
        public ListView ListViewComponents { get => listViewComponents; }
        Button IViewFormComponentEditing.ButtonRedactor => ButtonRedactor;
        Button IViewFormComponentEditing.ButtonAdd => ButtonAdd;
        Button IViewFormComponentEditing.ButtonDelete => ButtonDelete;

        public event EventHandler NameOfProductSelectedIndexEvent;
        public event EventHandler ButtonAddClickEvent;
        public event EventHandler ButtonDeleteEvent;
        public event EventHandler ButtonRedactorEvent;
        public event EventHandler ButtonCreateProductEvent;
        public event EventHandler ListViewComponentsMouseClickEvent;
        public event EventHandler ButtonSaveDataEvent;
        public event EventHandler FormComponentEditingLoadEvent;

        private void ComboBoxNameOfProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameOfProductSelectedIndexEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            ButtonAddClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            ButtonDeleteEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonRedactor_Click(object sender, EventArgs e)
        {
            ButtonRedactorEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonCreateProduct_Click(object sender, EventArgs e)
        {
            ButtonCreateProductEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ListViewComponents_MouseClick(object sender, MouseEventArgs e)
        {
            ListViewComponentsMouseClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonSaveData_Click(object sender, EventArgs e)
        {
            ButtonSaveDataEvent?.Invoke(this, EventArgs.Empty);
        }

        private void FormComponentEditing_Load(object sender, EventArgs e)
        {
            FormComponentEditingLoadEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}