﻿using System;
using System.Windows.Forms;

namespace Lesson3H2.Views
{
    interface IViewFormComponentEditing
    {
        ComboBox ComboBoxNameOfProducts { get; }
        TextBox TextBoxUnitCost { get; }
        TextBox TextBoxTotalCost { get; }
        ListView ListViewComponents { get; }
        Button ButtonRedactor { get; }
        Button ButtonAdd { get; }
        Button ButtonDelete { get; }

        event EventHandler NameOfProductSelectedIndexEvent;
        event EventHandler ButtonAddClickEvent;
        event EventHandler ButtonDeleteEvent;
        event EventHandler ButtonRedactorEvent;
        event EventHandler ButtonCreateProductEvent;
        event EventHandler ListViewComponentsMouseClickEvent;
        event EventHandler ButtonSaveDataEvent;
        event EventHandler FormComponentEditingLoadEvent;
    }
}