﻿namespace Lesson3H2.Views
{
    partial class FormComponentEditing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormComponentEditing));
            this.ComboBoxNameOfProducts = new System.Windows.Forms.ComboBox();
            this.TextBoxUnitCost = new System.Windows.Forms.TextBox();
            this.ButtonRedactor = new System.Windows.Forms.Button();
            this.LabelUnitCost = new System.Windows.Forms.Label();
            this.LabelTotalCost = new System.Windows.Forms.Label();
            this.TextBoxTotalCost = new System.Windows.Forms.TextBox();
            this.ButtonAdd = new System.Windows.Forms.Button();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.buttonCreateProduct = new System.Windows.Forms.Button();
            this.listViewComponents = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCharacteristics = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonSaveData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboBoxNameOfProducts
            // 
            this.ComboBoxNameOfProducts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxNameOfProducts.FormattingEnabled = true;
            this.ComboBoxNameOfProducts.Location = new System.Drawing.Point(12, 12);
            this.ComboBoxNameOfProducts.Name = "ComboBoxNameOfProducts";
            this.ComboBoxNameOfProducts.Size = new System.Drawing.Size(354, 21);
            this.ComboBoxNameOfProducts.TabIndex = 0;
            this.ComboBoxNameOfProducts.SelectedIndexChanged += new System.EventHandler(this.ComboBoxNameOfProducts_SelectedIndexChanged);
            // 
            // TextBoxUnitCost
            // 
            this.TextBoxUnitCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxUnitCost.Location = new System.Drawing.Point(129, 530);
            this.TextBoxUnitCost.Name = "TextBoxUnitCost";
            this.TextBoxUnitCost.ReadOnly = true;
            this.TextBoxUnitCost.Size = new System.Drawing.Size(181, 20);
            this.TextBoxUnitCost.TabIndex = 2;
            // 
            // ButtonRedactor
            // 
            this.ButtonRedactor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonRedactor.Enabled = false;
            this.ButtonRedactor.Location = new System.Drawing.Point(12, 583);
            this.ButtonRedactor.Name = "ButtonRedactor";
            this.ButtonRedactor.Size = new System.Drawing.Size(298, 23);
            this.ButtonRedactor.TabIndex = 3;
            this.ButtonRedactor.Text = "Редактировать товар";
            this.ButtonRedactor.UseVisualStyleBackColor = true;
            this.ButtonRedactor.Click += new System.EventHandler(this.ButtonRedactor_Click);
            // 
            // LabelUnitCost
            // 
            this.LabelUnitCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelUnitCost.AutoSize = true;
            this.LabelUnitCost.Location = new System.Drawing.Point(12, 533);
            this.LabelUnitCost.Name = "LabelUnitCost";
            this.LabelUnitCost.Size = new System.Drawing.Size(82, 15);
            this.LabelUnitCost.TabIndex = 4;
            this.LabelUnitCost.Text = "Цена товара\r\n";
            // 
            // LabelTotalCost
            // 
            this.LabelTotalCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelTotalCost.AutoSize = true;
            this.LabelTotalCost.Location = new System.Drawing.Point(15, 560);
            this.LabelTotalCost.Name = "LabelTotalCost";
            this.LabelTotalCost.Size = new System.Drawing.Size(112, 15);
            this.LabelTotalCost.TabIndex = 5;
            this.LabelTotalCost.Text = "Общая стоимость";
            // 
            // TextBoxTotalCost
            // 
            this.TextBoxTotalCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxTotalCost.Location = new System.Drawing.Point(129, 557);
            this.TextBoxTotalCost.Name = "TextBoxTotalCost";
            this.TextBoxTotalCost.ReadOnly = true;
            this.TextBoxTotalCost.Size = new System.Drawing.Size(180, 20);
            this.TextBoxTotalCost.TabIndex = 6;
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.Enabled = false;
            this.ButtonAdd.Location = new System.Drawing.Point(372, 12);
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.Size = new System.Drawing.Size(84, 23);
            this.ButtonAdd.TabIndex = 7;
            this.ButtonAdd.Text = "Добавить";
            this.ButtonAdd.UseVisualStyleBackColor = true;
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Enabled = false;
            this.ButtonDelete.Location = new System.Drawing.Point(372, 42);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(84, 23);
            this.ButtonDelete.TabIndex = 8;
            this.ButtonDelete.Text = "Удалить";
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // buttonCreateProduct
            // 
            this.buttonCreateProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCreateProduct.Location = new System.Drawing.Point(12, 613);
            this.buttonCreateProduct.Name = "buttonCreateProduct";
            this.buttonCreateProduct.Size = new System.Drawing.Size(298, 23);
            this.buttonCreateProduct.TabIndex = 10;
            this.buttonCreateProduct.Text = "Создать товар";
            this.buttonCreateProduct.UseVisualStyleBackColor = true;
            this.buttonCreateProduct.Click += new System.EventHandler(this.ButtonCreateProduct_Click);
            // 
            // listViewComponents
            // 
            this.listViewComponents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewComponents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnCharacteristics,
            this.columnPrice});
            this.listViewComponents.HideSelection = false;
            this.listViewComponents.Location = new System.Drawing.Point(462, 12);
            this.listViewComponents.Name = "listViewComponents";
            this.listViewComponents.Size = new System.Drawing.Size(837, 624);
            this.listViewComponents.TabIndex = 11;
            this.listViewComponents.UseCompatibleStateImageBehavior = false;
            this.listViewComponents.View = System.Windows.Forms.View.Details;
            this.listViewComponents.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListViewComponents_MouseClick);
            // 
            // columnName
            // 
            this.columnName.Text = "Название";
            this.columnName.Width = 350;
            // 
            // columnCharacteristics
            // 
            this.columnCharacteristics.Text = "Характеристики";
            this.columnCharacteristics.Width = 400;
            // 
            // columnPrice
            // 
            this.columnPrice.Text = "Цена";
            this.columnPrice.Width = 120;
            // 
            // buttonSaveData
            // 
            this.buttonSaveData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSaveData.Location = new System.Drawing.Point(333, 613);
            this.buttonSaveData.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonSaveData.Name = "buttonSaveData";
            this.buttonSaveData.Size = new System.Drawing.Size(106, 23);
            this.buttonSaveData.TabIndex = 12;
            this.buttonSaveData.Text = "Сохранить данные";
            this.buttonSaveData.UseVisualStyleBackColor = true;
            this.buttonSaveData.Click += new System.EventHandler(this.ButtonSaveData_Click);
            // 
            // FormComponentEditing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1311, 648);
            this.Controls.Add(this.buttonSaveData);
            this.Controls.Add(this.listViewComponents);
            this.Controls.Add(this.buttonCreateProduct);
            this.Controls.Add(this.ButtonDelete);
            this.Controls.Add(this.ButtonAdd);
            this.Controls.Add(this.TextBoxTotalCost);
            this.Controls.Add(this.LabelTotalCost);
            this.Controls.Add(this.LabelUnitCost);
            this.Controls.Add(this.ButtonRedactor);
            this.Controls.Add(this.TextBoxUnitCost);
            this.Controls.Add(this.ComboBoxNameOfProducts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1327, 687);
            this.Name = "FormComponentEditing";
            this.Text = "Учет продаж";
            this.Load += new System.EventHandler(this.FormComponentEditing_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxNameOfProducts;
        private System.Windows.Forms.TextBox TextBoxUnitCost;
        private System.Windows.Forms.Button ButtonRedactor;
        private System.Windows.Forms.Label LabelUnitCost;
        private System.Windows.Forms.Label LabelTotalCost;
        private System.Windows.Forms.TextBox TextBoxTotalCost;
        private System.Windows.Forms.Button ButtonAdd;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.Button buttonCreateProduct;
        private System.Windows.Forms.ListView listViewComponents;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnCharacteristics;
        private System.Windows.Forms.ColumnHeader columnPrice;
        private System.Windows.Forms.Button buttonSaveData;
    }
}