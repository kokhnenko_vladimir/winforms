﻿using System;
using System.Windows.Forms;

namespace Lesson3H2.Views
{
    public partial class FormCreateProduct : Form, IViewFormCreateProduct
    {
        public FormCreateProduct()
        {
            InitializeComponent();
        }

        public TextBox TextBoxName => textBoxName;

        public TextBox TextBoxCharacteristics => textBoxCharacteristics;

        public TextBox TextPrice => textBoxPrice;

        public Button ButtonAddProduct => buttonAddProduct;

        public event EventHandler ButtonAddProductEvent;
        public event EventHandler TextBoxNameChangeTextEvent;
        public event EventHandler TextBoxCharacteristicsChangeTextEvent;
        public event EventHandler TextBoxPriceChangeTextEvent;

        private void TextBoxName_TextChanged(object sender, EventArgs e)
        {
            TextBoxNameChangeTextEvent?.Invoke(this, EventArgs.Empty);
        }

        private void TextBoxCharacteristics_TextChanged(object sender, EventArgs e)
        {
            TextBoxCharacteristicsChangeTextEvent?.Invoke(this, EventArgs.Empty);
        }

        private void TextBoxPrice_TextChanged(object sender, EventArgs e)
        {
            TextBoxPriceChangeTextEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonAddProduct_Click(object sender, EventArgs e)
        {
            ButtonAddProductEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}