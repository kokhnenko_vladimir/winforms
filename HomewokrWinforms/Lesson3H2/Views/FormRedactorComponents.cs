﻿using System;
using System.Windows.Forms;

namespace Lesson3H2.Views
{
    public partial class FormRedactorComponents : Form, IViewFormSalesRecord
    {
        public FormRedactorComponents()
        {
            InitializeComponent();
        }

        public TextBox TextBoxRedactorNameProp { get => TextBoxRedactorName; set => TextBoxRedactorName = value; }
        public TextBox TextBoxRedactorCharacteristicsProp { get => TextBoxRedactorCharacteristics; set => TextBoxRedactorCharacteristics = value; }
        public TextBox TextBoxPriceProp { get => TextBoxPrice; set => TextBoxPrice = value; }
        public Button ButtonUndo => buttonUndo;
        public Button ButtonSave => buttonSave;

        public event EventHandler ButtonUndoEvent;
        public event EventHandler ButtonSaveEvent;
        public event EventHandler FormRedactorComponentsLoadEvent;
        public event EventHandler TextBoxPriceTextChangedEvent;

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            ButtonSaveEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonUndo_Click(object sender, EventArgs e)
        {
            ButtonUndoEvent?.Invoke(this, EventArgs.Empty);
        }

        private void FormRedactorComponents_Load(object sender, EventArgs e)
        {
            FormRedactorComponentsLoadEvent?.Invoke(this, EventArgs.Empty);
        }

        private void TextBoxPrice_TextChanged(object sender, EventArgs e)
        {
            TextBoxPriceTextChangedEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}