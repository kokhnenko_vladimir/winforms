﻿namespace Lesson3H2.Views
{
    partial class FormRedactorComponents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRedactorComponents));
            this.LabelRedactorName = new System.Windows.Forms.Label();
            this.TextBoxRedactorName = new System.Windows.Forms.TextBox();
            this.TextBoxRedactorCharacteristics = new System.Windows.Forms.TextBox();
            this.LabelCharacteristics = new System.Windows.Forms.Label();
            this.LabelRedactorPrice = new System.Windows.Forms.Label();
            this.TextBoxPrice = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonUndo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelRedactorName
            // 
            this.LabelRedactorName.AutoSize = true;
            this.LabelRedactorName.Location = new System.Drawing.Point(12, 9);
            this.LabelRedactorName.Name = "LabelRedactorName";
            this.LabelRedactorName.Size = new System.Drawing.Size(57, 13);
            this.LabelRedactorName.TabIndex = 3;
            this.LabelRedactorName.Text = "Название";
            // 
            // TextBoxRedactorName
            // 
            this.TextBoxRedactorName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxRedactorName.Location = new System.Drawing.Point(12, 25);
            this.TextBoxRedactorName.Multiline = true;
            this.TextBoxRedactorName.Name = "TextBoxRedactorName";
            this.TextBoxRedactorName.Size = new System.Drawing.Size(1200, 60);
            this.TextBoxRedactorName.TabIndex = 4;
            // 
            // TextBoxRedactorCharacteristics
            // 
            this.TextBoxRedactorCharacteristics.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxRedactorCharacteristics.Location = new System.Drawing.Point(15, 107);
            this.TextBoxRedactorCharacteristics.Multiline = true;
            this.TextBoxRedactorCharacteristics.Name = "TextBoxRedactorCharacteristics";
            this.TextBoxRedactorCharacteristics.Size = new System.Drawing.Size(1197, 160);
            this.TextBoxRedactorCharacteristics.TabIndex = 6;
            // 
            // LabelCharacteristics
            // 
            this.LabelCharacteristics.AutoSize = true;
            this.LabelCharacteristics.Location = new System.Drawing.Point(12, 91);
            this.LabelCharacteristics.Name = "LabelCharacteristics";
            this.LabelCharacteristics.Size = new System.Drawing.Size(90, 13);
            this.LabelCharacteristics.TabIndex = 5;
            this.LabelCharacteristics.Text = "Характеристики";
            // 
            // LabelRedactorPrice
            // 
            this.LabelRedactorPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelRedactorPrice.AutoSize = true;
            this.LabelRedactorPrice.Location = new System.Drawing.Point(12, 270);
            this.LabelRedactorPrice.Name = "LabelRedactorPrice";
            this.LabelRedactorPrice.Size = new System.Drawing.Size(33, 13);
            this.LabelRedactorPrice.TabIndex = 9;
            this.LabelRedactorPrice.Text = "Цена";
            // 
            // TextBoxPrice
            // 
            this.TextBoxPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxPrice.Location = new System.Drawing.Point(15, 286);
            this.TextBoxPrice.Multiline = true;
            this.TextBoxPrice.Name = "TextBoxPrice";
            this.TextBoxPrice.Size = new System.Drawing.Size(534, 20);
            this.TextBoxPrice.TabIndex = 11;
            this.TextBoxPrice.TextChanged += new System.EventHandler(this.TextBoxPrice_TextChanged);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(1030, 286);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 12;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonUndo
            // 
            this.buttonUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUndo.Location = new System.Drawing.Point(1137, 286);
            this.buttonUndo.Name = "buttonUndo";
            this.buttonUndo.Size = new System.Drawing.Size(75, 23);
            this.buttonUndo.TabIndex = 13;
            this.buttonUndo.Text = "Отменить";
            this.buttonUndo.UseVisualStyleBackColor = true;
            this.buttonUndo.Click += new System.EventHandler(this.ButtonUndo_Click);
            // 
            // FormRedactorComponents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 318);
            this.Controls.Add(this.buttonUndo);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.TextBoxPrice);
            this.Controls.Add(this.LabelRedactorPrice);
            this.Controls.Add(this.TextBoxRedactorCharacteristics);
            this.Controls.Add(this.LabelCharacteristics);
            this.Controls.Add(this.TextBoxRedactorName);
            this.Controls.Add(this.LabelRedactorName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1240, 357);
            this.Name = "FormRedactorComponents";
            this.Text = "Редактирование комплектующих";
            this.Load += new System.EventHandler(this.FormRedactorComponents_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LabelRedactorName;
        private System.Windows.Forms.TextBox TextBoxRedactorName;
        private System.Windows.Forms.TextBox TextBoxRedactorCharacteristics;
        private System.Windows.Forms.Label LabelCharacteristics;
        private System.Windows.Forms.Label LabelRedactorPrice;
        private System.Windows.Forms.TextBox TextBoxPrice;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonUndo;
    }
}