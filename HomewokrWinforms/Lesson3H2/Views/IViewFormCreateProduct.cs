﻿using System;
using System.Windows.Forms;

namespace Lesson3H2.Views
{
    interface IViewFormCreateProduct
    {
        TextBox TextBoxName { get; }
        TextBox TextBoxCharacteristics { get; }
        TextBox TextPrice { get; }
        Button ButtonAddProduct { get; }

        event EventHandler ButtonAddProductEvent;
        event EventHandler TextBoxNameChangeTextEvent;
        event EventHandler TextBoxCharacteristicsChangeTextEvent;
        event EventHandler TextBoxPriceChangeTextEvent;
    }
}