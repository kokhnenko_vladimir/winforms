﻿using System;
using System.Windows.Forms;

namespace Lesson3H2.Views
{
    interface IViewFormSalesRecord
    {
        TextBox TextBoxRedactorNameProp { get; }
        TextBox TextBoxRedactorCharacteristicsProp { get; }
        TextBox TextBoxPriceProp { get; }
        Button ButtonUndo { get; }
        Button ButtonSave { get; }

        event EventHandler ButtonUndoEvent;
        event EventHandler ButtonSaveEvent;
        event EventHandler FormRedactorComponentsLoadEvent;
        event EventHandler TextBoxPriceTextChangedEvent;
    }
}