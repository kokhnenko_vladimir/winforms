﻿using System;
using System.IO;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace Lesson5H1
{
    public partial class FormExplorer : Form
    {
        public FormExplorer()
        {
            InitializeComponent();
        }

        private void TreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            GetAllNodesForTheTreeNode(e.Node);
        }

        private void GetAllNodesForTheTreeNode(TreeNode mainNode)
        {
            try
            {
                mainNode.Nodes.Clear();
                listView.Items.Clear();

                DirectoryInfo nodeDirInfo = new DirectoryInfo(mainNode.Tag.ToString());
                ListViewSubItem[] subItems;

                foreach (DirectoryInfo dir in nodeDirInfo.GetDirectories())
                {
                    var item = new ListViewItem(dir.Name, 1)
                    {
                        Tag = dir.FullName
                    };

                    subItems = new ListViewItem.ListViewSubItem[]
                    {
                        new ListViewItem.ListViewSubItem(item, "Directory"),
                        new ListViewItem.ListViewSubItem(item, dir.LastAccessTime.ToShortDateString())
                    };
                    item.SubItems.AddRange(subItems);
                    listView.Items.Add(item);

                    TreeNode node = new TreeNode
                    {
                        Tag = dir.FullName,
                        Text = dir.Name,
                        ImageIndex = 1,
                        SelectedImageIndex = 1
                    };
                    FillTreeNode(node, dir.FullName);
                    mainNode.Nodes.Add(node);
                }

                foreach (FileInfo file in nodeDirInfo.GetFiles())
                {
                    var item = new ListViewItem(file.Name, 2)
                    {
                        Tag = file.FullName
                    };

                    subItems = new ListViewItem.ListViewSubItem[]
                    {
                        new ListViewItem.ListViewSubItem(item, "File"),
                        new ListViewItem.ListViewSubItem(item, file.LastAccessTime.ToShortDateString())
                    };

                    item.SubItems.AddRange(subItems);
                    listView.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void FillTreeNode(TreeNode driveNode, string path)
        {
            try
            {
                DirectoryInfo nodeDirInfo = new DirectoryInfo(path);

                foreach (DirectoryInfo dir in nodeDirInfo.GetDirectories())
                {
                    driveNode.Nodes.Add(new TreeNode(dir.Name, 1, 1) { Tag = dir.FullName });
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void FormExplorer_Load(object sender, EventArgs e)
        {
            try
            {
                DriveInfo[] info = DriveInfo.GetDrives();

                for (int i = 0; i < info.Length; i++)
                {
                    TreeNode node = new TreeNode
                    {
                        Tag = info[i].Name,
                        Text = info[i].Name.Trim('\\') + " " + info[i].VolumeLabel
                    };
                    FillTreeNode(node, info[i].Name);
                    treeView.Nodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }
    }
}
