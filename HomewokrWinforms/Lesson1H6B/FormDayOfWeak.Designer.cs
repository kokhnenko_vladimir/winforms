﻿namespace Lesson1H6
{
    partial class FormDayOfWeak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.labelDayOfWeak = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // maskedTextBox
            // 
            this.maskedTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.maskedTextBox.Location = new System.Drawing.Point(124, 34);
            this.maskedTextBox.Mask = "00/00/0000";
            this.maskedTextBox.Name = "maskedTextBox";
            this.maskedTextBox.Size = new System.Drawing.Size(63, 20);
            this.maskedTextBox.TabIndex = 0;
            this.maskedTextBox.ValidatingType = typeof(System.DateTime);
            this.maskedTextBox.TextChanged += new System.EventHandler(this.MaskedTextBox_TextChanged);
            // 
            // labelDayOfWeak
            // 
            this.labelDayOfWeak.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDayOfWeak.Location = new System.Drawing.Point(226, 35);
            this.labelDayOfWeak.Name = "labelDayOfWeak";
            this.labelDayOfWeak.Size = new System.Drawing.Size(111, 19);
            this.labelDayOfWeak.TabIndex = 1;
            this.labelDayOfWeak.Text = "Result";
            this.labelDayOfWeak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormDayOfWeak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 89);
            this.Controls.Add(this.labelDayOfWeak);
            this.Controls.Add(this.maskedTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "FormDayOfWeak";
            this.Text = "Определение дня недели";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox maskedTextBox;
        private System.Windows.Forms.Label labelDayOfWeak;
    }
}

