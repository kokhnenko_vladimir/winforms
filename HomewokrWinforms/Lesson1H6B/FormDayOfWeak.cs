﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Windows.Forms;

namespace Lesson1H6
{
    public partial class FormDayOfWeak : Form
    {
        private readonly List<Pair> daysOfWeak;
        public FormDayOfWeak()
        {
            InitializeComponent();

            daysOfWeak = new List<Pair>
                {
                    new Pair("Monday", "Понедельник"),
                    new Pair("Tuesday", "Вторник"),
                    new Pair("Wednesday", "Среда"),
                    new Pair("Thursday", "Четверг"),
                    new Pair("Friday", "Пятница"),
                    new Pair("Saturday", "Суббота"),
                    new Pair("Sunday", "Воскресенье")
                };
        }

        private void MaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            if (maskedTextBox.MaskCompleted)
            {
                try
                {
                    labelDayOfWeak.Text = Convert.ToDateTime(maskedTextBox.Text).DayOfWeek.ToString();

                    foreach (var item in daysOfWeak)
                    {
                        if (labelDayOfWeak.Text == item.First.ToString())
                            labelDayOfWeak.Text = item.Second.ToString();
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Некорректный ввод данных");
                    maskedTextBox.Text = string.Empty;
                }
            }
            else
            {
                labelDayOfWeak.Text = "Result";
            }
        }
    }
}
