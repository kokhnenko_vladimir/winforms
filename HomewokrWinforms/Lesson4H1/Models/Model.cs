﻿using NLog;

namespace Lesson4H1.Models
{
    public class Model : IModel
    {
        public string DocName { get; set; }
        public string StringForCompare { get; set; }
        public Logger Logger { get; set; } = LogManager.GetCurrentClassLogger();
    }
}