﻿using NLog;

namespace Lesson4H1.Models
{
    public interface IModel
    {
        string DocName { get; set; }
        string StringForCompare { get; set; }
        Logger Logger { get; set; }
    }
}
