﻿using Lesson4H1.Models;
using Lesson4H1.Presenters;
using Lesson4H1.Views;
using System;
using System.Windows.Forms;

namespace Lesson4H1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IViewMainForm mainForm = new MainForm();
            new MainPresenter(mainForm, new Model());
            Application.Run((Form)mainForm);
        }
    }
}