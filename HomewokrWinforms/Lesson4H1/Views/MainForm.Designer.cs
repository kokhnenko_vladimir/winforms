﻿namespace Lesson4H1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripCut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuEditSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripFormat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripFont = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripColorFont = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripColorBack = new System.Windows.Forms.ToolStripMenuItem();
            this.richText = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cutToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFile,
            this.toolStripEdit,
            this.toolStripFormat});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(800, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // toolStripFile
            // 
            this.toolStripFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripCreate,
            this.toolStripOpen,
            this.toolStripSave,
            this.toolStripSaveAs});
            this.toolStripFile.Name = "toolStripFile";
            this.toolStripFile.Size = new System.Drawing.Size(37, 20);
            this.toolStripFile.Text = "&File";
            this.toolStripFile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // toolStripCreate
            // 
            this.toolStripCreate.Name = "toolStripCreate";
            this.toolStripCreate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripCreate.Size = new System.Drawing.Size(195, 22);
            this.toolStripCreate.Text = "&Create";
            this.toolStripCreate.Click += new System.EventHandler(this.ToolStripCreate_Click);
            // 
            // toolStripOpen
            // 
            this.toolStripOpen.Name = "toolStripOpen";
            this.toolStripOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.toolStripOpen.Size = new System.Drawing.Size(195, 22);
            this.toolStripOpen.Text = "&Open";
            this.toolStripOpen.Click += new System.EventHandler(this.ToolStripOpen_Click);
            // 
            // toolStripSave
            // 
            this.toolStripSave.Name = "toolStripSave";
            this.toolStripSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripSave.Size = new System.Drawing.Size(195, 22);
            this.toolStripSave.Text = "&Save";
            this.toolStripSave.Click += new System.EventHandler(this.ToolStripSave_Click);
            // 
            // toolStripSaveAs
            // 
            this.toolStripSaveAs.Name = "toolStripSaveAs";
            this.toolStripSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.toolStripSaveAs.Size = new System.Drawing.Size(195, 22);
            this.toolStripSaveAs.Text = "Save &As...";
            this.toolStripSaveAs.Click += new System.EventHandler(this.ToolStripSaveAs_Click);
            // 
            // toolStripEdit
            // 
            this.toolStripEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripCut,
            this.toolStripCopy,
            this.toolStripPaste,
            this.toolStripUndo,
            this.toolStripDelete,
            this.toolStripMenuEditSeparator,
            this.toolStripSelectAll});
            this.toolStripEdit.Name = "toolStripEdit";
            this.toolStripEdit.Size = new System.Drawing.Size(39, 20);
            this.toolStripEdit.Text = "&Edit";
            // 
            // toolStripCut
            // 
            this.toolStripCut.Name = "toolStripCut";
            this.toolStripCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.toolStripCut.Size = new System.Drawing.Size(164, 22);
            this.toolStripCut.Text = "Cu&t";
            this.toolStripCut.Click += new System.EventHandler(this.ToolStripCut_Click);
            // 
            // toolStripCopy
            // 
            this.toolStripCopy.Name = "toolStripCopy";
            this.toolStripCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.toolStripCopy.Size = new System.Drawing.Size(164, 22);
            this.toolStripCopy.Text = "&Copy";
            this.toolStripCopy.Click += new System.EventHandler(this.ToolStripCopy_Click);
            // 
            // toolStripPaste
            // 
            this.toolStripPaste.Name = "toolStripPaste";
            this.toolStripPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.toolStripPaste.Size = new System.Drawing.Size(164, 22);
            this.toolStripPaste.Text = "&Paste";
            this.toolStripPaste.Click += new System.EventHandler(this.ToolStripPaste_Click);
            // 
            // toolStripUndo
            // 
            this.toolStripUndo.Name = "toolStripUndo";
            this.toolStripUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.toolStripUndo.Size = new System.Drawing.Size(164, 22);
            this.toolStripUndo.Text = "&Undo";
            this.toolStripUndo.Click += new System.EventHandler(this.ToolStripUndo_Click);
            // 
            // toolStripDelete
            // 
            this.toolStripDelete.Name = "toolStripDelete";
            this.toolStripDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.toolStripDelete.Size = new System.Drawing.Size(164, 22);
            this.toolStripDelete.Text = "&Delete";
            this.toolStripDelete.Click += new System.EventHandler(this.ToolStripDelete_Click);
            // 
            // toolStripMenuEditSeparator
            // 
            this.toolStripMenuEditSeparator.Name = "toolStripMenuEditSeparator";
            this.toolStripMenuEditSeparator.Size = new System.Drawing.Size(161, 6);
            // 
            // toolStripSelectAll
            // 
            this.toolStripSelectAll.Name = "toolStripSelectAll";
            this.toolStripSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.toolStripSelectAll.Size = new System.Drawing.Size(164, 22);
            this.toolStripSelectAll.Text = "&Select All";
            this.toolStripSelectAll.Click += new System.EventHandler(this.ToolStripSelectAll_Click);
            // 
            // toolStripFormat
            // 
            this.toolStripFormat.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFont,
            this.toolStripColorFont,
            this.ToolStripColorBack});
            this.toolStripFormat.Name = "toolStripFormat";
            this.toolStripFormat.Size = new System.Drawing.Size(57, 20);
            this.toolStripFormat.Text = "F&ormat";
            // 
            // toolStripFont
            // 
            this.toolStripFont.Name = "toolStripFont";
            this.toolStripFont.Size = new System.Drawing.Size(131, 22);
            this.toolStripFont.Text = "&Font";
            this.toolStripFont.Click += new System.EventHandler(this.ToolStripFont_Click);
            // 
            // toolStripColorFont
            // 
            this.toolStripColorFont.Name = "toolStripColorFont";
            this.toolStripColorFont.Size = new System.Drawing.Size(131, 22);
            this.toolStripColorFont.Text = "&Color Font";
            this.toolStripColorFont.Click += new System.EventHandler(this.ToolStripColorFont_Click);
            // 
            // ToolStripColorBack
            // 
            this.ToolStripColorBack.Name = "ToolStripColorBack";
            this.ToolStripColorBack.Size = new System.Drawing.Size(131, 22);
            this.ToolStripColorBack.Text = "Color &Back";
            this.ToolStripColorBack.Click += new System.EventHandler(this.ToolStripColorBack_Click);
            // 
            // richText
            // 
            this.richText.ContextMenuStrip = this.contextMenuStrip;
            this.richText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richText.Location = new System.Drawing.Point(0, 24);
            this.richText.Name = "richText";
            this.richText.Size = new System.Drawing.Size(788, 426);
            this.richText.TabIndex = 1;
            this.richText.Text = "";
            this.richText.SelectionChanged += new System.EventHandler(this.RichText_SelectionChanged);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStrip,
            this.copyToolStrip,
            this.pasteToolStrip,
            this.undoToolStrip,
            this.deleteToolStrip});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(147, 114);
            // 
            // cutToolStrip
            // 
            this.cutToolStrip.Name = "cutToolStrip";
            this.cutToolStrip.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStrip.Size = new System.Drawing.Size(146, 22);
            this.cutToolStrip.Text = "Cu&t";
            this.cutToolStrip.Click += new System.EventHandler(this.ToolStripCut_Click);
            // 
            // copyToolStrip
            // 
            this.copyToolStrip.Name = "copyToolStrip";
            this.copyToolStrip.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStrip.Size = new System.Drawing.Size(146, 22);
            this.copyToolStrip.Text = "&Copy";
            this.copyToolStrip.Click += new System.EventHandler(this.ToolStripCopy_Click);
            // 
            // pasteToolStrip
            // 
            this.pasteToolStrip.Name = "pasteToolStrip";
            this.pasteToolStrip.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStrip.Size = new System.Drawing.Size(146, 22);
            this.pasteToolStrip.Text = "&Paste";
            this.pasteToolStrip.Click += new System.EventHandler(this.ToolStripPaste_Click);
            // 
            // undoToolStrip
            // 
            this.undoToolStrip.Name = "undoToolStrip";
            this.undoToolStrip.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.undoToolStrip.Size = new System.Drawing.Size(146, 22);
            this.undoToolStrip.Text = "&Undo";
            this.undoToolStrip.Click += new System.EventHandler(this.ToolStripUndo_Click);
            // 
            // deleteToolStrip
            // 
            this.deleteToolStrip.Name = "deleteToolStrip";
            this.deleteToolStrip.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStrip.Size = new System.Drawing.Size(146, 22);
            this.deleteToolStrip.Text = "&Delete";
            this.deleteToolStrip.Click += new System.EventHandler(this.ToolStripDelete_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.richText);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "MainForm";
            this.Text = "Text Edit";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripCreate;
        private System.Windows.Forms.ToolStripMenuItem toolStripOpen;
        private System.Windows.Forms.ToolStripMenuItem toolStripSave;
        private System.Windows.Forms.ToolStripMenuItem toolStripSaveAs;
        private System.Windows.Forms.ToolStripMenuItem toolStripEdit;
        private System.Windows.Forms.ToolStripMenuItem toolStripCut;
        private System.Windows.Forms.ToolStripMenuItem toolStripUndo;
        private System.Windows.Forms.ToolStripMenuItem toolStripFormat;
        private System.Windows.Forms.ToolStripMenuItem toolStripFont;
        private System.Windows.Forms.RichTextBox richText;
        private System.Windows.Forms.ToolStripMenuItem toolStripCopy;
        private System.Windows.Forms.ToolStripMenuItem toolStripPaste;
        private System.Windows.Forms.ToolStripMenuItem toolStripDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuEditSeparator;
        private System.Windows.Forms.ToolStripMenuItem toolStripSelectAll;
        private System.Windows.Forms.ToolStripMenuItem toolStripColorFont;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem cutToolStrip;
        private System.Windows.Forms.ToolStripMenuItem copyToolStrip;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStrip;
        private System.Windows.Forms.ToolStripMenuItem undoToolStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripColorBack;
    }
}

