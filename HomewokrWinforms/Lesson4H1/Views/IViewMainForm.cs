﻿using System;
using System.Windows.Forms;

namespace Lesson4H1.Views
{
    public interface IViewMainForm
    {
        RichTextBox MyRichTextBox { get; set; }
        string TextForm { get; set; }
        ToolStripMenuItem MainMenuSellectAll { get; }
        ToolStripMenuItem MainMenuCut { get; }
        ToolStripMenuItem MainMenuCopy { get; }
        ToolStripMenuItem MainMenuPaste { get; }
        ToolStripMenuItem MainMenuUndo { get; }
        ToolStripMenuItem MainMenuDelete { get; }

        ToolStripMenuItem ContextMenuCut { get; }
        ToolStripMenuItem ContextMenuPaste { get; }
        ToolStripMenuItem ContextMenuCopy { get; }
        ToolStripMenuItem ContextMenuUndo { get; }
        ToolStripMenuItem ContextMenuDelete { get; }

        event EventHandler ToolStripCreateClickEvent;
        event EventHandler ToolStripOpenClickEvent;
        event EventHandler ToolStripSaveClickEvent;
        event EventHandler ToolStripSaveAsClickEvent;
        event EventHandler ToolStripCutClickEvent;
        event EventHandler ToolStripCopyClickEvent;
        event EventHandler ToolStripPasteClickEvent;
        event EventHandler ToolStripUndoClickEvent;
        event EventHandler ToolStripDeleteClickEvent;
        event EventHandler ToolStripSelectAllClickEvent;
        event EventHandler ToolStripFontClickEvent;
        event EventHandler ToolStripColorFontClickEvent;
        event EventHandler ToolStripColorBackClickEvent;
        event EventHandler RichTextSelectionChangedEvent;
    }
}