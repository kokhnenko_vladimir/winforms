﻿using Lesson4H1.Views;
using System;
using System.Windows.Forms;

namespace Lesson4H1
{
    public partial class MainForm : Form, IViewMainForm
    {
        public RichTextBox MyRichTextBox { get => richText; set => richText = value; }
        public string TextForm { get => this.Text; set => this.Text = value; }

        public ToolStripMenuItem MainMenuSellectAll => toolStripSelectAll;

        public ToolStripMenuItem MainMenuCut => toolStripCut;

        public ToolStripMenuItem MainMenuCopy => toolStripCopy;

        public ToolStripMenuItem MainMenuPaste => toolStripPaste;

        public ToolStripMenuItem MainMenuUndo => toolStripUndo;

        public ToolStripMenuItem MainMenuDelete => toolStripDelete;

        public ToolStripMenuItem ContextMenuCut => cutToolStrip;

        public ToolStripMenuItem ContextMenuPaste => pasteToolStrip;

        public ToolStripMenuItem ContextMenuCopy => copyToolStrip;

        public ToolStripMenuItem ContextMenuUndo => undoToolStrip;

        public ToolStripMenuItem ContextMenuDelete => deleteToolStrip;

        public MainForm()
        {
            InitializeComponent();

            cutToolStrip.Enabled = false;
            copyToolStrip.Enabled = false;
            pasteToolStrip.Enabled = false;
            deleteToolStrip.Enabled = false;
            undoToolStrip.Enabled = false;

            toolStripCut.Enabled = false;
            toolStripCopy.Enabled = false;
            toolStripPaste.Enabled = false;
            toolStripDelete.Enabled = false;
            toolStripUndo.Enabled = false;

            toolStripSelectAll.Enabled = false;
        }

        public event EventHandler ToolStripCreateClickEvent;
        public event EventHandler ToolStripOpenClickEvent;
        public event EventHandler ToolStripSaveClickEvent;
        public event EventHandler ToolStripSaveAsClickEvent;
        public event EventHandler ToolStripCutClickEvent;
        public event EventHandler ToolStripCopyClickEvent;
        public event EventHandler ToolStripPasteClickEvent;
        public event EventHandler ToolStripUndoClickEvent;
        public event EventHandler ToolStripDeleteClickEvent;
        public event EventHandler ToolStripSelectAllClickEvent;
        public event EventHandler ToolStripFontClickEvent;
        public event EventHandler ToolStripColorFontClickEvent;
        public event EventHandler ToolStripColorBackClickEvent;
        public event EventHandler RichTextSelectionChangedEvent;

        //
        // File
        //

        private void ToolStripCreate_Click(object sender, EventArgs e)
        {
            ToolStripCreateClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripOpen_Click(object sender, EventArgs e)
        {
            ToolStripOpenClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripSave_Click(object sender, EventArgs e)
        {
            ToolStripSaveClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripSaveAs_Click(object sender, EventArgs e)
        {
            ToolStripSaveAsClickEvent?.Invoke(this, EventArgs.Empty);
        }

        //
        // Edit
        //

        private void ToolStripCut_Click(object sender, EventArgs e)
        {
            ToolStripCutClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripCopy_Click(object sender, EventArgs e)
        {
            ToolStripCopyClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripPaste_Click(object sender, EventArgs e)
        {
            ToolStripPasteClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripUndo_Click(object sender, EventArgs e)
        {
            ToolStripUndoClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripDelete_Click(object sender, EventArgs e)
        {
            ToolStripDeleteClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripSelectAll_Click(object sender, EventArgs e)
        {
            ToolStripSelectAllClickEvent?.Invoke(this, EventArgs.Empty);
        }

        //
        // Format
        //

        private void ToolStripFont_Click(object sender, EventArgs e)
        {
            ToolStripFontClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolStripColorFont_Click(object sender, EventArgs e)
        {
            ToolStripColorFontClickEvent?.Invoke(this, EventArgs.Empty);
        }
        private void ToolStripColorBack_Click(object sender, EventArgs e)
        {
            ToolStripColorBackClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void RichText_SelectionChanged(object sender, EventArgs e)
        {
            RichTextSelectionChangedEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}