﻿using Lesson4H1.Models;
using Lesson4H1.Views;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Lesson4H1.Presenters
{
    public class MainPresenter
    {
        private readonly IViewMainForm _view;
        private readonly IModel _model;

        public MainPresenter(IViewMainForm view, IModel model)
        {
            _view = view;
            _model = model;

            _view.ToolStripCreateClickEvent += ToolStripCreateClick;
            _view.ToolStripOpenClickEvent += ToolStripOpenClick;
            _view.ToolStripSaveClickEvent += ToolStripSaveClick;
            _view.ToolStripSaveAsClickEvent += ToolStripSaveAsClick;
            _view.ToolStripCutClickEvent += ToolStripCutClick;
            _view.ToolStripCopyClickEvent += ToolStripCopyClick;
            _view.ToolStripPasteClickEvent += ToolStripPasteClick;
            _view.ToolStripUndoClickEvent += ToolStripUndoClick;
            _view.ToolStripDeleteClickEvent += ToolStripDeleteClick;
            _view.ToolStripSelectAllClickEvent += ToolStripSelectAllClick;
            _view.ToolStripFontClickEvent += ToolStripFontClick;
            _view.ToolStripColorFontClickEvent += ToolStripColorFontClick;
            _view.ToolStripColorBackClickEvent += ToolStripColorBackClick;
            _view.RichTextSelectionChangedEvent += RichTextSelectionChanged;

        }

        //
        // File
        //

        private void ToolStripCreateClick(object sender, EventArgs e)
        {
            int result = String.Compare(_model.StringForCompare, _view.MyRichTextBox.Text);
            if (!string.IsNullOrEmpty(_model.DocName) && result != 0)
            {
                if (MessageBox.Show(
                    "Save changes in file?", string.Empty, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ToolStripSaveClick(this, EventArgs.Empty);
                }
            }

            _view.MyRichTextBox.Text = string.Empty;
            _view.MyRichTextBox.Text = string.Empty;
            _view.MyRichTextBox.Font = RichTextBox.DefaultFont;
            _view.MyRichTextBox.BackColor = Color.White;
            _view.MyRichTextBox.ForeColor = RichTextBox.DefaultForeColor;
            _model.DocName = string.Empty;
            _view.TextForm = "Новый документ";
        }

        private async void ToolStripOpenClick(object sender, EventArgs e)
        {
            try
            {
                int result = String.Compare(_model.StringForCompare, _view.MyRichTextBox.Text);
                if (!string.IsNullOrEmpty(_model.DocName) && result != 0)
                {
                    if (MessageBox.Show(
                        "Save changes in file?", string.Empty, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        ToolStripSaveClick(this, EventArgs.Empty);
                    }
                }

                OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Filter = "Text Files (*.txt)|*.txt|RTF Files(*.rtf)|*.rtf",
                    FilterIndex = 1
                };

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog.FileName.Length != 0)
                    {
                        if (Path.GetExtension(openFileDialog.FileName) == ".rtf")
                        {
                            _view.MyRichTextBox.LoadFile(openFileDialog.FileName);
                            _model.StringForCompare = _view.MyRichTextBox.Text;
                            _model.DocName = openFileDialog.FileName;
                            _view.TextForm = Path.GetFileName(openFileDialog.FileName);
                        }
                        else
                        {
                            using (var sr = new StreamReader(openFileDialog.FileName, System.Text.Encoding.Default))
                            {
                                _view.MyRichTextBox.Text = await sr.ReadToEndAsync();
                                _model.StringForCompare = _view.MyRichTextBox.Text;
                                _model.DocName = openFileDialog.FileName;
                                _view.TextForm = Path.GetFileName(openFileDialog.FileName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        private async void ToolStripSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(_model.DocName))
                {
                    if (Path.GetExtension(_model.DocName) == ".rtf")
                    {
                        _view.MyRichTextBox.SaveFile(_model.DocName);
                        _view.TextForm = Path.GetFileName(_model.DocName);
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(
                            _model.DocName, false, System.Text.Encoding.Default))
                        {
                            await sw.WriteLineAsync(_view.MyRichTextBox.Text);
                            _view.TextForm = Path.GetFileName(_model.DocName);
                        }
                    }
                }
                else
                {
                    ToolStripSaveAsClick(this, EventArgs.Empty);
                }

            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        private async void ToolStripSaveAsClick(object sender, EventArgs e)
        {
            try
            {
                var saveFileDialog = new SaveFileDialog
                {
                    Filter = "Text files (*.txt)|*.txt|RTF files (*.rtf)|*.rtf"
                };
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (Path.GetExtension(saveFileDialog.FileName) == ".rtf")
                    {
                        _view.MyRichTextBox.SaveFile(saveFileDialog.FileName);
                        _view.TextForm = Path.GetFileName(saveFileDialog.FileName);
                        _model.DocName = saveFileDialog.FileName;
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(
                            saveFileDialog.FileName, false, System.Text.Encoding.Default))
                        {
                            await sw.WriteLineAsync(_view.MyRichTextBox.Text);
                            _view.TextForm = Path.GetFileName(saveFileDialog.FileName);
                            _model.DocName = saveFileDialog.FileName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        //
        // Edit
        //

        private void ToolStripCutClick(object sender, EventArgs e)
        {
            _view.MyRichTextBox.Cut();
        }

        private void ToolStripCopyClick(object sender, EventArgs e)
        {
            _view.MyRichTextBox.Copy();
        }

        private void ToolStripPasteClick(object sender, EventArgs e)
        {
            _view.MyRichTextBox.Paste();
        }

        private void ToolStripUndoClick(object sender, EventArgs e)
        {
            _view.MyRichTextBox.Undo();
        }

        private void ToolStripDeleteClick(object sender, EventArgs e)
        {
            _view.MyRichTextBox.SelectedText = string.Empty;
        }

        private void ToolStripSelectAllClick(object sender, EventArgs e)
        {
            _view.MyRichTextBox.SelectAll();
        }

        //
        // Format
        //

        private void ToolStripFontClick(object sender, EventArgs e)
        {
            FontDialog _fontDialog = new FontDialog
            {
                Font = _view.MyRichTextBox.SelectionFont
            };

            if (_fontDialog.ShowDialog() == DialogResult.OK)
                _view.MyRichTextBox.SelectionFont = _fontDialog.Font;
        }

        private void ToolStripColorFontClick(object sender, EventArgs e)
        {
            ColorDialog _colorDialog = new ColorDialog
            {
                Color = _view.MyRichTextBox.SelectionColor,
                FullOpen = true
            };

            if (_colorDialog.ShowDialog() == DialogResult.OK)
                _view.MyRichTextBox.SelectionColor = _colorDialog.Color;
        }

        private void ToolStripColorBackClick(object sender, EventArgs e)
        {
            ColorDialog _colorDialog = new ColorDialog
            {
                Color = _view.MyRichTextBox.SelectionColor,
                FullOpen = true
            };

            if (_colorDialog.ShowDialog() == DialogResult.OK)
                _view.MyRichTextBox.SelectionBackColor = _colorDialog.Color;
        }

        //
        // RichTextBox
        //

        //
        // General
        //

        private void RichTextSelectionChanged(object sender, EventArgs e)
        {
            if (_view.MyRichTextBox.Text.Length > 0)
            {
                _view.MainMenuSellectAll.Enabled = true;
            }
            else
            {
                _view.MainMenuSellectAll.Enabled = false;
            }

            if (_view.MyRichTextBox.SelectedText.Length != 0)
            {
                _view.ContextMenuCopy.Enabled = true;
                _view.ContextMenuCut.Enabled = true;
                _view.ContextMenuDelete.Enabled = true;
                _view.ContextMenuPaste.Enabled = true;
                _view.ContextMenuUndo.Enabled = true;

                _view.MainMenuCopy.Enabled = true;
                _view.MainMenuCut.Enabled = true;
                _view.MainMenuDelete.Enabled = true;
                _view.MainMenuPaste.Enabled = true;
                _view.MainMenuUndo.Enabled = true;
            }
            else
            {
                _view.ContextMenuCopy.Enabled = false;
                _view.ContextMenuCut.Enabled = false;
                _view.ContextMenuDelete.Enabled = false;
                _view.ContextMenuPaste.Enabled = false;
                _view.ContextMenuUndo.Enabled = false;

                _view.MainMenuCopy.Enabled = false;
                _view.MainMenuCut.Enabled = false;
                _view.MainMenuDelete.Enabled = false;
                _view.MainMenuPaste.Enabled = false;
                _view.MainMenuUndo.Enabled = false;
            }
        }
    }
}