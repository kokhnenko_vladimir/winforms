﻿using System.Drawing;
using System.Windows.Forms;

namespace Lesson1H3
{
    public partial class FormRectangle : System.Windows.Forms.Form
    {
        public FormRectangle()
        {
            InitializeComponent();
        }

        private void Rectangle_MouseClick(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control && e.Button == MouseButtons.Left)
                Close();
            else if (e.Button == MouseButtons.Left)
                MessageBox.Show("Внутри");
            else if (e.Y == 0 && e.X >= 0 && e.X <= labelRectangle.Size.Width ||
                e.Y == labelRectangle.Height - 3 && e.X >= 0 && e.X <= labelRectangle.Size.Width ||
                e.X == 0 && e.Y >= 0 && e.Y <= labelRectangle.Size.Height ||
                e.X == labelRectangle.Size.Width - 3 && e.Y >= 0 && e.Y <= labelRectangle.Size.Height)
                MessageBox.Show("На рамке");
            else if (e.Button == MouseButtons.Right)
                Text = "Ширина = " + Size.Width.ToString() + ", Высота = " + Size.Height.ToString();
        }

        private void Form_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                MessageBox.Show("Снаружи");
            else if (e.Button == MouseButtons.Right)
                Text = "Ширина = " + Size.Width.ToString() + ", Высота = " + Size.Height.ToString();
        }

        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            Text = "X = " + e.X + ", Y = " + e.Y;
        }
    }
}
