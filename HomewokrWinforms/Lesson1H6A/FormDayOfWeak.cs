﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Windows.Forms;

namespace Lesson1H6A
{
    public partial class FormDayOfWeak : Form
    {
        private readonly List<Pair> daysOfWeak;
        private readonly Regex regex;
        public FormDayOfWeak()
        {
            InitializeComponent();

            daysOfWeak = new List<Pair>
                {
                    new Pair("Monday", "Понедельник"),
                    new Pair("Tuesday", "Вторник"),
                    new Pair("Wednesday", "Среда"),
                    new Pair("Thursday", "Четверг"),
                    new Pair("Friday", "Пятница"),
                    new Pair("Saturday", "Суббота"),
                    new Pair("Sunday", "Воскресенье")
                };

            regex = new Regex(@"\d{2}-\d{2}-\d{4}");
        }

        private void TextBoxInput_TextChanged(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 2 || textBoxInput.TextLength == 5)
            {
                textBoxInput.Text += "-";
                textBoxInput.SelectionStart = textBoxInput.TextLength;
            }
            else if (textBoxInput.TextLength == 10)
            {
                if (regex.IsMatch(textBoxInput.Text))
                {
                    try
                    {
                        labelResult.Text = Convert.ToDateTime(textBoxInput.Text).DayOfWeek.ToString();
                        foreach (var item in daysOfWeak)
                        {
                            if (labelResult.Text == item.First.ToString())
                                labelResult.Text = item.Second.ToString();
                        }
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("Некорректный ввод");
                        textBoxInput.Clear();
                        labelResult.Text = "Result";
                    }
                }
                else
                {
                    textBoxInput.Clear();
                }
            }
            else if (textBoxInput.TextLength == 11)
            {
                textBoxInput.Clear();
                labelResult.Text = "Result";
            }
        }

        private void FormDayOfWeak_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }
}
