﻿namespace Lesson2H2
{
    partial class MainFormProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBoxProfile = new System.Windows.Forms.GroupBox();
            this.textBoxForSkillsDevelopment = new System.Windows.Forms.TextBox();
            this.labelForSkillsDevelopment = new System.Windows.Forms.Label();
            this.labelForEducation2 = new System.Windows.Forms.Label();
            this.labelForEducation1 = new System.Windows.Forms.Label();
            this.textBoxForEducationFacility = new System.Windows.Forms.TextBox();
            this.labelForEducationFacility = new System.Windows.Forms.Label();
            this.comboBoxForEducationSphere = new System.Windows.Forms.ComboBox();
            this.comboBoxForEducation = new System.Windows.Forms.ComboBox();
            this.textBoxForPhone = new System.Windows.Forms.TextBox();
            this.labelForPhone = new System.Windows.Forms.Label();
            this.textBoxForAddress = new System.Windows.Forms.TextBox();
            this.labelForAddress = new System.Windows.Forms.Label();
            this.textBoxForBirth = new System.Windows.Forms.TextBox();
            this.labelForBirth = new System.Windows.Forms.Label();
            this.labelForPatronymic = new System.Windows.Forms.Label();
            this.textBoxForPatronymic = new System.Windows.Forms.TextBox();
            this.labelForName = new System.Windows.Forms.Label();
            this.textBoxForName = new System.Windows.Forms.TextBox();
            this.labelForSurname = new System.Windows.Forms.Label();
            this.textBoxForSurname = new System.Windows.Forms.TextBox();
            this.groupBoxForOutput = new System.Windows.Forms.GroupBox();
            this.labelForOutput = new System.Windows.Forms.Label();
            this.GroupBoxProfile.SuspendLayout();
            this.groupBoxForOutput.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBoxProfile
            // 
            this.GroupBoxProfile.Controls.Add(this.textBoxForSkillsDevelopment);
            this.GroupBoxProfile.Controls.Add(this.labelForSkillsDevelopment);
            this.GroupBoxProfile.Controls.Add(this.labelForEducation2);
            this.GroupBoxProfile.Controls.Add(this.labelForEducation1);
            this.GroupBoxProfile.Controls.Add(this.textBoxForEducationFacility);
            this.GroupBoxProfile.Controls.Add(this.labelForEducationFacility);
            this.GroupBoxProfile.Controls.Add(this.comboBoxForEducationSphere);
            this.GroupBoxProfile.Controls.Add(this.comboBoxForEducation);
            this.GroupBoxProfile.Controls.Add(this.textBoxForPhone);
            this.GroupBoxProfile.Controls.Add(this.labelForPhone);
            this.GroupBoxProfile.Controls.Add(this.textBoxForAddress);
            this.GroupBoxProfile.Controls.Add(this.labelForAddress);
            this.GroupBoxProfile.Controls.Add(this.textBoxForBirth);
            this.GroupBoxProfile.Controls.Add(this.labelForBirth);
            this.GroupBoxProfile.Controls.Add(this.labelForPatronymic);
            this.GroupBoxProfile.Controls.Add(this.textBoxForPatronymic);
            this.GroupBoxProfile.Controls.Add(this.labelForName);
            this.GroupBoxProfile.Controls.Add(this.textBoxForName);
            this.GroupBoxProfile.Controls.Add(this.labelForSurname);
            this.GroupBoxProfile.Controls.Add(this.textBoxForSurname);
            this.GroupBoxProfile.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GroupBoxProfile.Location = new System.Drawing.Point(10, 11);
            this.GroupBoxProfile.Margin = new System.Windows.Forms.Padding(2);
            this.GroupBoxProfile.Name = "GroupBoxProfile";
            this.GroupBoxProfile.Padding = new System.Windows.Forms.Padding(2);
            this.GroupBoxProfile.Size = new System.Drawing.Size(427, 493);
            this.GroupBoxProfile.TabIndex = 0;
            this.GroupBoxProfile.TabStop = false;
            this.GroupBoxProfile.Text = "Ввод";
            // 
            // textBoxForSkillsDevelopment
            // 
            this.textBoxForSkillsDevelopment.Location = new System.Drawing.Point(5, 435);
            this.textBoxForSkillsDevelopment.Multiline = true;
            this.textBoxForSkillsDevelopment.Name = "textBoxForSkillsDevelopment";
            this.textBoxForSkillsDevelopment.Size = new System.Drawing.Size(417, 60);
            this.textBoxForSkillsDevelopment.TabIndex = 22;
            this.textBoxForSkillsDevelopment.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForSkillsDevelopment
            // 
            this.labelForSkillsDevelopment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForSkillsDevelopment.Location = new System.Drawing.Point(5, 399);
            this.labelForSkillsDevelopment.Name = "labelForSkillsDevelopment";
            this.labelForSkillsDevelopment.Size = new System.Drawing.Size(417, 29);
            this.labelForSkillsDevelopment.TabIndex = 21;
            this.labelForSkillsDevelopment.Text = "Дополнительное образование, повышение квалификации (указать наименование курсов, " +
    "срок обучения, полеченную специальность):";
            // 
            // labelForEducation2
            // 
            this.labelForEducation2.AutoSize = true;
            this.labelForEducation2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForEducation2.Location = new System.Drawing.Point(5, 252);
            this.labelForEducation2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForEducation2.Name = "labelForEducation2";
            this.labelForEducation2.Size = new System.Drawing.Size(111, 15);
            this.labelForEducation2.TabIndex = 20;
            this.labelForEducation2.Text = "Сфера образования";
            // 
            // labelForEducation1
            // 
            this.labelForEducation1.AutoSize = true;
            this.labelForEducation1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForEducation1.Location = new System.Drawing.Point(5, 219);
            this.labelForEducation1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForEducation1.Name = "labelForEducation1";
            this.labelForEducation1.Size = new System.Drawing.Size(77, 15);
            this.labelForEducation1.TabIndex = 19;
            this.labelForEducation1.Text = "Образование";
            // 
            // textBoxForEducationFacility
            // 
            this.textBoxForEducationFacility.Location = new System.Drawing.Point(5, 331);
            this.textBoxForEducationFacility.Multiline = true;
            this.textBoxForEducationFacility.Name = "textBoxForEducationFacility";
            this.textBoxForEducationFacility.Size = new System.Drawing.Size(417, 60);
            this.textBoxForEducationFacility.TabIndex = 18;
            this.textBoxForEducationFacility.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForEducationFacility
            // 
            this.labelForEducationFacility.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForEducationFacility.Location = new System.Drawing.Point(5, 280);
            this.labelForEducationFacility.Name = "labelForEducationFacility";
            this.labelForEducationFacility.Size = new System.Drawing.Size(417, 41);
            this.labelForEducationFacility.TabIndex = 17;
            this.labelForEducationFacility.Text = "Какое учебное заведение Вы окончили\r\n(указать полное наименование учебного заведе" +
    "ния, факультет, специальность по диплому, срок обучения):";
            // 
            // comboBoxForEducationSphere
            // 
            this.comboBoxForEducationSphere.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxForEducationSphere.FormattingEnabled = true;
            this.comboBoxForEducationSphere.Items.AddRange(new object[] {
            "Гуманитарное",
            "Естественнонаучное",
            "Техническое",
            "Экономическое"});
            this.comboBoxForEducationSphere.Location = new System.Drawing.Point(126, 249);
            this.comboBoxForEducationSphere.Name = "comboBoxForEducationSphere";
            this.comboBoxForEducationSphere.Size = new System.Drawing.Size(296, 21);
            this.comboBoxForEducationSphere.Sorted = true;
            this.comboBoxForEducationSphere.TabIndex = 16;
            this.comboBoxForEducationSphere.SelectedIndexChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // comboBoxForEducation
            // 
            this.comboBoxForEducation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxForEducation.FormattingEnabled = true;
            this.comboBoxForEducation.Items.AddRange(new object[] {
            "Высшее профессиональное(ВУЗ)",
            "Начальное профессиональное(ПТУ)",
            "Незаконченное высшее",
            "Общее(школа, лицей)",
            "Среднее профессиональное (техникум, колледж и пр.)"});
            this.comboBoxForEducation.Location = new System.Drawing.Point(126, 216);
            this.comboBoxForEducation.Name = "comboBoxForEducation";
            this.comboBoxForEducation.Size = new System.Drawing.Size(296, 21);
            this.comboBoxForEducation.Sorted = true;
            this.comboBoxForEducation.TabIndex = 15;
            this.comboBoxForEducation.SelectedIndexChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // textBoxForPhone
            // 
            this.textBoxForPhone.Location = new System.Drawing.Point(126, 184);
            this.textBoxForPhone.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxForPhone.Name = "textBoxForPhone";
            this.textBoxForPhone.Size = new System.Drawing.Size(296, 20);
            this.textBoxForPhone.TabIndex = 11;
            this.textBoxForPhone.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForPhone
            // 
            this.labelForPhone.AutoSize = true;
            this.labelForPhone.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForPhone.Location = new System.Drawing.Point(5, 187);
            this.labelForPhone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForPhone.Name = "labelForPhone";
            this.labelForPhone.Size = new System.Drawing.Size(62, 15);
            this.labelForPhone.TabIndex = 10;
            this.labelForPhone.Text = "Телефоны";
            // 
            // textBoxForAddress
            // 
            this.textBoxForAddress.Location = new System.Drawing.Point(126, 140);
            this.textBoxForAddress.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxForAddress.Multiline = true;
            this.textBoxForAddress.Name = "textBoxForAddress";
            this.textBoxForAddress.Size = new System.Drawing.Size(296, 33);
            this.textBoxForAddress.TabIndex = 9;
            this.textBoxForAddress.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForAddress
            // 
            this.labelForAddress.AutoSize = true;
            this.labelForAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForAddress.Location = new System.Drawing.Point(5, 143);
            this.labelForAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForAddress.Name = "labelForAddress";
            this.labelForAddress.Size = new System.Drawing.Size(104, 15);
            this.labelForAddress.TabIndex = 8;
            this.labelForAddress.Text = "Место жительства";
            // 
            // textBoxForBirth
            // 
            this.textBoxForBirth.Location = new System.Drawing.Point(126, 113);
            this.textBoxForBirth.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxForBirth.Name = "textBoxForBirth";
            this.textBoxForBirth.Size = new System.Drawing.Size(296, 20);
            this.textBoxForBirth.TabIndex = 7;
            this.textBoxForBirth.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForBirth
            // 
            this.labelForBirth.AutoSize = true;
            this.labelForBirth.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForBirth.Location = new System.Drawing.Point(5, 118);
            this.labelForBirth.Name = "labelForBirth";
            this.labelForBirth.Size = new System.Drawing.Size(89, 15);
            this.labelForBirth.TabIndex = 6;
            this.labelForBirth.Text = "Дата Рождения";
            // 
            // labelForPatronymic
            // 
            this.labelForPatronymic.AutoSize = true;
            this.labelForPatronymic.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForPatronymic.Location = new System.Drawing.Point(5, 86);
            this.labelForPatronymic.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForPatronymic.Name = "labelForPatronymic";
            this.labelForPatronymic.Size = new System.Drawing.Size(56, 15);
            this.labelForPatronymic.TabIndex = 5;
            this.labelForPatronymic.Text = "Отчество";
            // 
            // textBoxForPatronymic
            // 
            this.textBoxForPatronymic.Location = new System.Drawing.Point(126, 80);
            this.textBoxForPatronymic.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxForPatronymic.Name = "textBoxForPatronymic";
            this.textBoxForPatronymic.Size = new System.Drawing.Size(296, 20);
            this.textBoxForPatronymic.TabIndex = 4;
            this.textBoxForPatronymic.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForName
            // 
            this.labelForName.AutoSize = true;
            this.labelForName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForName.Location = new System.Drawing.Point(5, 54);
            this.labelForName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForName.Name = "labelForName";
            this.labelForName.Size = new System.Drawing.Size(31, 15);
            this.labelForName.TabIndex = 3;
            this.labelForName.Text = "Имя";
            // 
            // textBoxForName
            // 
            this.textBoxForName.Location = new System.Drawing.Point(126, 49);
            this.textBoxForName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxForName.Name = "textBoxForName";
            this.textBoxForName.Size = new System.Drawing.Size(296, 20);
            this.textBoxForName.TabIndex = 2;
            this.textBoxForName.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // labelForSurname
            // 
            this.labelForSurname.AutoSize = true;
            this.labelForSurname.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForSurname.Location = new System.Drawing.Point(5, 22);
            this.labelForSurname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelForSurname.Name = "labelForSurname";
            this.labelForSurname.Size = new System.Drawing.Size(58, 15);
            this.labelForSurname.TabIndex = 1;
            this.labelForSurname.Text = "Фамилия";
            // 
            // textBoxForSurname
            // 
            this.textBoxForSurname.Location = new System.Drawing.Point(126, 19);
            this.textBoxForSurname.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxForSurname.Name = "textBoxForSurname";
            this.textBoxForSurname.Size = new System.Drawing.Size(296, 20);
            this.textBoxForSurname.TabIndex = 0;
            this.textBoxForSurname.TextChanged += new System.EventHandler(this.TextBoxForName_TextChanged);
            // 
            // groupBoxForOutput
            // 
            this.groupBoxForOutput.Controls.Add(this.labelForOutput);
            this.groupBoxForOutput.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBoxForOutput.Location = new System.Drawing.Point(441, 11);
            this.groupBoxForOutput.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxForOutput.Name = "groupBoxForOutput";
            this.groupBoxForOutput.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxForOutput.Size = new System.Drawing.Size(427, 493);
            this.groupBoxForOutput.TabIndex = 23;
            this.groupBoxForOutput.TabStop = false;
            this.groupBoxForOutput.Text = "Вывод";
            // 
            // labelForOutput
            // 
            this.labelForOutput.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelForOutput.Location = new System.Drawing.Point(5, 16);
            this.labelForOutput.Name = "labelForOutput";
            this.labelForOutput.Size = new System.Drawing.Size(417, 472);
            this.labelForOutput.TabIndex = 0;
            // 
            // MainFormProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 513);
            this.Controls.Add(this.groupBoxForOutput);
            this.Controls.Add(this.GroupBoxProfile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(891, 552);
            this.MinimumSize = new System.Drawing.Size(891, 552);
            this.Name = "MainFormProfile";
            this.Text = "Анкета";
            this.GroupBoxProfile.ResumeLayout(false);
            this.GroupBoxProfile.PerformLayout();
            this.groupBoxForOutput.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBoxProfile;
        private System.Windows.Forms.Label labelForSurname;
        private System.Windows.Forms.TextBox textBoxForSurname;
        private System.Windows.Forms.Label labelForPatronymic;
        private System.Windows.Forms.TextBox textBoxForPatronymic;
        private System.Windows.Forms.Label labelForName;
        private System.Windows.Forms.TextBox textBoxForName;
        private System.Windows.Forms.Label labelForBirth;
        private System.Windows.Forms.TextBox textBoxForBirth;
        private System.Windows.Forms.TextBox textBoxForAddress;
        private System.Windows.Forms.Label labelForAddress;
        private System.Windows.Forms.TextBox textBoxForPhone;
        private System.Windows.Forms.Label labelForPhone;
        private System.Windows.Forms.ComboBox comboBoxForEducation;
        private System.Windows.Forms.Label labelForEducationFacility;
        private System.Windows.Forms.ComboBox comboBoxForEducationSphere;
        private System.Windows.Forms.TextBox textBoxForEducationFacility;
        private System.Windows.Forms.Label labelForEducation2;
        private System.Windows.Forms.Label labelForEducation1;
        private System.Windows.Forms.TextBox textBoxForSkillsDevelopment;
        private System.Windows.Forms.Label labelForSkillsDevelopment;
        private System.Windows.Forms.GroupBox groupBoxForOutput;
        private System.Windows.Forms.Label labelForOutput;
    }
}

