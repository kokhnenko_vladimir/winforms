﻿using System;
using System.Windows.Forms;

namespace Lesson2H2
{
    public partial class MainFormProfile : Form
    {
        public MainFormProfile()
        {
            InitializeComponent();
            comboBoxForEducation.SelectedIndex = 0;
            comboBoxForEducationSphere.SelectedIndex = 0;
        }

        private void TextBoxForName_TextChanged(object sender, EventArgs e)
        {
            labelForOutput.Text = textBoxForSurname.Text +
                " " + textBoxForName.Text +
                " " + textBoxForPatronymic.Text +
                " " + textBoxForBirth.Text +
                "\n" + textBoxForAddress.Text +
                "\n(Тел.) - " + textBoxForPhone.Text;

            if (comboBoxForEducation.SelectedItem != null)
                labelForOutput.Text = labelForOutput.Text +
                    "\n" + comboBoxForEducation.SelectedItem.ToString();

            if (comboBoxForEducationSphere.SelectedItem != null)
                labelForOutput.Text = labelForOutput.Text +
                    "\n" + comboBoxForEducationSphere.SelectedItem;

            labelForOutput.Text = labelForOutput.Text +
                "\n" + textBoxForEducationFacility.Text +
                "\n" + textBoxForSkillsDevelopment.Text;
        }
    }
}
