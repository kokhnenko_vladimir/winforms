﻿using Lesson4H2.Models;
using Lesson4H2.Views;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;


namespace Lesson4H2.Presenters
{
    public class FormFileExplorerPresenter
    {
        private readonly IMainFormFileExplorer _view;
        private readonly IModel _model;

        //private TreeNode _parentNode;

        //private readonly List<string> _pathForCopy = new List<string>();
        //private readonly List<string> _pathForCut = new List<string>();
        //private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public FormFileExplorerPresenter(IMainFormFileExplorer view, IModel model)
        {
            _view = view;
            _model = model;

            _view.MainFormFileExplorerLoadEvent += MainFormFileExplorerLoad;
            _view.TreeViewNodeClickEvent += TreeViewNodeClick;
            _view.TreeViewBeforeExpandEvent += TreeViewBeforeExpand;
            _view.ListViewSelectedChange += DeactivationOfKeywInContextMenuAndToolStripMenu;
            _view.DeleteFileOrFolderEvent += DeleteFileOrFolderEvent;
            _view.TreeViewAfterSelectEvent += TreeViewAfterSelect;
            _view.TreeViewKeyDownEvent += TreeViewKeyDown;
            _view.ListViewKeyDownEvent += ListViewKeyDown;
            _view.ListViewMouseDoubleClickEvent += ListViewMouseDoubleClick;
            _view.MenuOpenClickEvent += MenuOpenClick;
            _view.ToolBarButtonOpenClickEvent += ToolBarButtonOpenClick;
            _view.ContextMenuOpenClickEvent += ContextMenuOpenClick;
            _view.ToolBarButtonBackClickEvent += ToolBarButtonBackClick;
            _view.GeneralCutClickEvent += GeneralCutClick;
            _view.GeneralPasteClickEvent += GeneralPasteClick;
            _view.GeneralCopyClickEvent += GeneralCopyClick;
        }

        //
        //Buttons
        //

        private void DeleteFileOrFolderEvent(object sender, EventArgs e)
        {
            if (_view.MyListView.SelectedItems.Count > 0 &&
                MessageBox.Show($"You really want to irretrievably remove objects in quantity" +
                $" {_view.MyListView.SelectedItems.Count}", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                try
                {
                    foreach (ListViewItem item in _view.MyListView.SelectedItems)
                    {

                        if (item.SubItems[1].Text == "Directory")
                        {
                            Directory.Delete(item.Tag.ToString(), true);

                            if (_model.ParentNode != null)
                            {
                                foreach (TreeNode node in _model.ParentNode.Nodes)
                                {
                                    if (node != null && node.Tag.ToString() == item.Tag.ToString())
                                        node.Remove();
                                }
                            }
                        }
                        else
                        {
                            new FileInfo(item.Tag.ToString()).Delete();
                        }

                        _view.MyListView.Items.Remove(item);
                    }
                }
                catch (Exception ex)
                {
                    _model.Logger.Error(ex);
                }
            }
        }

        private void MenuOpenClick(object sender, EventArgs e)
        {
            ForOpenFolderOrFile();
        }

        private void ToolBarButtonOpenClick(object sender, EventArgs e)
        {
            ForOpenFolderOrFile();
        }

        private void ContextMenuOpenClick(object sender, EventArgs e)
        {
            ForOpenFolderOrFile();
        }

        private void ToolBarButtonBackClick(object sender, EventArgs e)
        {
            GetAllNodesForTheTreeNode(_model.ParentNode.Parent);
        }

        private void GeneralCutClick(object sender, EventArgs e)
        {
            _model.PathForCut.Clear();

            foreach (ListViewItem item in _view.MyListView.SelectedItems)
            {
                _model.PathForCut.Add(item.Tag.ToString());
            }
        }

        private void GeneralCopyClick(object sender, EventArgs eventArgs)
        {
            _model.PathForCopy.Clear();

            foreach (ListViewItem item in _view.MyListView.SelectedItems)
            {
                _model.PathForCopy.Add(item.Tag.ToString());
            }
        }

        private void GeneralPasteClick(object sender, EventArgs e)
        {
            try
            {
                if (_model.PathForCopy.Count > 0)
                {
                    CopyDirrectoiresAndFiles();
                }
                else if (_model.PathForCut.Count > 0)
                {
                    CutDirrectoriesAndFiles();

                    _model.PathForCut.Clear();
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        //
        // ListView events
        //

        private void ListViewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.A))
            {
                CtrA();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Esc();
            }
            else if (e.KeyCode == Keys.Back)
            {
                _view.MyTreeView.Focus();
            }
            else if (e.KeyCode == Keys.Enter && _view.MyListView.SelectedItems.Count == 1)
            {
                ForOpenFolderOrFile();
            }
        }

        private void ListViewMouseDoubleClick(object sender, MouseEventArgs e)
        {
            ForOpenFolderOrFile();
        }

        //
        // TreeView events
        //

        private void MainFormFileExplorerLoad(object sender, EventArgs eventArgs)
        {
            try
            {
                DriveInfo[] info = DriveInfo.GetDrives();

                for (int i = 0; i < info.Length; i++)
                {
                    TreeNode node = new TreeNode
                    {
                        Tag = info[i].Name,
                        Text = info[i].Name.Trim('\\') + " " + info[i].VolumeLabel
                    };
                    FillTreeNode(node, info[i].Name);
                    _view.MyTreeView.Nodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        private void TreeViewBeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            GetAllNodesForTheTreeNode(e.Node);
        }


        private void TreeViewNodeClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            GetAllNodesForTheTreeNode(e.Node);
        }

        private void TreeViewAfterSelect(object sender, TreeViewEventArgs e)
        {
            _model.ParentNode = e.Node;
        }

        private void TreeViewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                GetAllNodesForTheTreeNode(_model.ParentNode);
            }
        }

        //
        // General
        //

        private void GetAllNodesForTheTreeNode(TreeNode mainNode)
        {
            try
            {
                mainNode.Nodes.Clear();
                _view.MyListView.Items.Clear();

                DirectoryInfo nodeDirInfo = new DirectoryInfo(mainNode.Tag.ToString());
                ListViewSubItem[] subItems;

                foreach (DirectoryInfo dir in nodeDirInfo.GetDirectories())
                {
                    var item = new ListViewItem(dir.Name, 1)
                    {
                        Tag = dir.FullName
                    };

                    subItems = new ListViewItem.ListViewSubItem[]
                    {
                        new ListViewItem.ListViewSubItem(item, "Directory"),
                        new ListViewItem.ListViewSubItem(item, dir.LastAccessTime.ToShortDateString())
                    };
                    item.SubItems.AddRange(subItems);
                    _view.MyListView.Items.Add(item);

                    TreeNode node = new TreeNode
                    {
                        Tag = dir.FullName,
                        Text = dir.Name,
                        ImageIndex = 1,
                        SelectedImageIndex = 1
                    };
                    FillTreeNode(node, dir.FullName);
                    mainNode.Nodes.Add(node);
                }

                foreach (FileInfo file in nodeDirInfo.GetFiles())
                {
                    var item = new ListViewItem(file.Name, 2)
                    {
                        Tag = file.FullName
                    };

                    subItems = new ListViewItem.ListViewSubItem[]
                    {
                        new ListViewItem.ListViewSubItem(item, "File"),
                        new ListViewItem.ListViewSubItem(item, file.LastAccessTime.ToShortDateString())
                    };

                    item.SubItems.AddRange(subItems);
                    _view.MyListView.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        private void FillTreeNode(TreeNode driveNode, string path)
        {
            try
            {
                DirectoryInfo nodeDirInfo = new DirectoryInfo(path);

                foreach (DirectoryInfo dir in nodeDirInfo.GetDirectories())
                {
                    driveNode.Nodes.Add(new TreeNode(dir.Name, 1, 1) { Tag = dir.FullName });
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        private void DeactivationOfKeywInContextMenuAndToolStripMenu(object sender, EventArgs e)
        {
            if (_view.MyListView.SelectedItems.Count > 0)
            {
                _view.ContextMenuCopy.Enabled = true;
                _view.ContextMenuCut.Enabled = true;
                _view.ContextMenuDelete.Enabled = true;
                _view.ContextMenuPaste.Enabled = true;
                _view.ContextMenuOpen.Enabled = true;

                _view.MenuCopy.Enabled = true;
                _view.MenuCut.Enabled = true;
                _view.MenuDelete.Enabled = true;
                _view.MenuOpen.Enabled = true;
                _view.MenuPaste.Enabled = true;

                _view.ToolBarButtonCopy.Enabled = true;
                _view.ToolBarButtonCut.Enabled = true;
                _view.ToolBarButtonDelete.Enabled = true;
                _view.ToolBarButtonOpen.Enabled = true;
                _view.ToolBarButtonPaste.Enabled = true;
            }
            else
            {
                _view.ContextMenuCopy.Enabled = false;
                _view.ContextMenuCut.Enabled = false;
                _view.ContextMenuDelete.Enabled = false;
                _view.ContextMenuPaste.Enabled = false;
                _view.ContextMenuOpen.Enabled = false;

                _view.MenuCopy.Enabled = false;
                _view.MenuCut.Enabled = false;
                _view.MenuDelete.Enabled = false;
                _view.MenuOpen.Enabled = false;
                _view.MenuPaste.Enabled = false;

                _view.ToolBarButtonCopy.Enabled = false;
                _view.ToolBarButtonCut.Enabled = false;
                _view.ToolBarButtonDelete.Enabled = false;
                _view.ToolBarButtonOpen.Enabled = false;
                _view.ToolBarButtonPaste.Enabled = false;
            }
        }

        private void CtrA()
        {
            for (int i = 0; i < _view.MyListView.Items.Count; i++)
            {
                _view.MyListView.Items[i].Selected = true;
            }
        }

        private void Esc()
        {
            for (int i = 0; i < _view.MyListView.Items.Count; i++)
            {
                _view.MyListView.Items[i].Selected = false;
            }
        }

        private void ForOpenFolderOrFile()
        {
            try
            {
                if (Directory.Exists(_view.MyListView.SelectedItems[0].Tag.ToString()))
                {
                    for (int i = 0; i < _model.ParentNode.Nodes.Count; i++)
                    {
                        if (string.Compare(
                            _model.ParentNode.Nodes[i].Tag.ToString(),
                            _view.MyListView.SelectedItems[0].Tag.ToString()) == 0)
                        {
                            _model.ParentNode.Expand();
                            _model.ParentNode.Nodes[i].Expand();
                            TreeNode nodeTmp = _model.ParentNode.Nodes[i];
                            _model.ParentNode = nodeTmp;
                            _view.MyTreeView.SelectedNode = nodeTmp;
                            GetAllNodesForTheTreeNode(_model.ParentNode);
                            break;
                        }
                    }
                }
                else if (File.Exists(_view.MyListView.SelectedItems[0].Tag.ToString()))
                {
                    Task.Factory.StartNew(() =>
                    {
                        if (_view.MyListView.InvokeRequired)
                        {
                            _view.MyListView.BeginInvoke(new Action(() =>
                            {
                                Process.Start(_view.MyListView.SelectedItems[0].Tag.ToString());
                            }));
                            Thread.Sleep(5000);
                            GetAllNodesForTheTreeNode(_model.ParentNode);
                        }
                        else
                        {
                            Process.Start(_view.MyListView.SelectedItems[0].Tag.ToString());
                            Thread.Sleep(5000);
                            GetAllNodesForTheTreeNode(_model.ParentNode);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                _model.Logger.Error(ex);
            }
        }

        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!Directory.Exists(destDirName))
            {
                if (!Directory.Exists(destDirName))
                {
                    Directory.CreateDirectory(destDirName);
                }
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        private void CopyDirrectoiresAndFiles()
        {
            foreach (var pathForCopy in _model.PathForCopy)
            {
                if (Directory.Exists(pathForCopy))
                {
                    string[] folders = Directory.GetDirectories(_view.MyTreeView.SelectedNode.Tag.ToString());
                    bool didItWork = true;

                    foreach (var folder in folders)
                    {
                        if (string.Compare(new DirectoryInfo(folder).Name, new DirectoryInfo(pathForCopy).Name) == 0)
                        {
                            string nameCurrentCopyFolder = new DirectoryInfo(pathForCopy).Name;
                            string[] destFolders = Directory.GetDirectories(
                                                    _view.MyTreeView.SelectedNode.Tag.ToString());

                            Regex regex = new Regex(@"Copy\((\d+)\)");
                            int max = 0;

                            foreach (var destFolder in destFolders)
                            {
                                string destFolderName = new DirectoryInfo(destFolder).Name;

                                if (regex.IsMatch(destFolderName))
                                {
                                    string str = Regex.Replace(destFolderName, @" Copy\((\d+)\)", string.Empty);

                                    if (string.Compare(str, nameCurrentCopyFolder) == 0)
                                    {
                                        Match match = regex.Match(destFolderName);
                                        int tmp = Convert.ToInt32(match.Groups[1].Value);

                                        if (max < tmp)
                                            max = tmp;
                                    }
                                }
                            }
                            string destDirName = Path.Combine(_view.MyTreeView.SelectedNode.Tag.ToString(),
                            string.Concat(new DirectoryInfo(pathForCopy).Name, $" Copy({++max})"));

                            Directory.CreateDirectory(destDirName);
                            DirectoryCopy(pathForCopy, destDirName, true);
                            GetAllNodesForTheTreeNode(_model.ParentNode);
                            didItWork = false;
                            break;
                        }
                    }

                    if (didItWork)
                    {
                        string destDirName = Path.Combine(
                                  _view.MyTreeView.SelectedNode.Tag.ToString(),
                                  new DirectoryInfo(pathForCopy).Name);

                        DirectoryCopy(pathForCopy, destDirName, true);
                        GetAllNodesForTheTreeNode(_model.ParentNode);
                    }
                }
                else if (File.Exists(pathForCopy))
                {
                    string[] files = Directory.GetFiles(_view.MyTreeView.SelectedNode.Tag.ToString());
                    bool didItWork = true;

                    foreach (var file in files)
                    {
                        if (string.Compare(
                            new FileInfo(file).Name, new FileInfo(pathForCopy).Name) == 0)
                        {
                            try
                            {
                                File.Copy(pathForCopy,
                                Path.Combine(_view.MyTreeView.SelectedNode.Tag.ToString(),
                                    string.Concat(
                                        string.Concat(
                                        new FileInfo(pathForCopy).Name.Replace(
                                            new FileInfo(pathForCopy).Extension, string.Empty), " Copy(0)"),
                                            new FileInfo(pathForCopy).Extension)), false);

                            }
                            catch (IOException)
                            {
                                string nameCurrentCopyFile = new FileInfo(pathForCopy).Name;
                                string[] destFiles = Directory.GetFiles(
                                                        _view.MyTreeView.SelectedNode.Tag.ToString());

                                Regex regex = new Regex(@"Copy\((\d+)\)\.");
                                int max = 0;

                                foreach (var destFile in destFiles)
                                {
                                    string destFileName = new FileInfo(destFile).Name;

                                    if (regex.IsMatch(destFileName))
                                    {
                                        string str = Regex.Replace(destFileName, @" Copy\((\d+)\)\.", ".");

                                        if (string.Compare(str, nameCurrentCopyFile) == 0)
                                        {
                                            Match match = regex.Match(destFileName);
                                            int tmp = Convert.ToInt32(match.Groups[1].Value);

                                            if (max < tmp)
                                                max = tmp;
                                        }
                                    }
                                }

                                File.Copy(pathForCopy,
                                Path.Combine(_view.MyTreeView.SelectedNode.Tag.ToString(),
                                    string.Concat(
                                        string.Concat(
                                        new FileInfo(pathForCopy).Name.Replace(
                                            new FileInfo(pathForCopy).Extension, string.Empty), $" Copy({++max})"),
                                            new FileInfo(pathForCopy).Extension)), false);
                            }

                            didItWork = false;
                            GetAllNodesForTheTreeNode(_model.ParentNode);
                            break;
                        }
                    }

                    if (didItWork)
                    {
                        File.Copy(pathForCopy,
                                Path.Combine(
                                    _view.MyTreeView.SelectedNode.Tag.ToString(),
                                    new FileInfo(pathForCopy).Name), false);

                        GetAllNodesForTheTreeNode(_model.ParentNode);
                    }
                }
            }
        }

        private void CutDirrectoriesAndFiles()
        {
            string[] folders = Directory.GetDirectories(_view.MyTreeView.SelectedNode.Tag.ToString());

            foreach (var cutOutFolder in _model.PathForCut)
            {
                bool didItWork = true;

                foreach (var folder in folders)
                {
                    if (Directory.Exists(cutOutFolder))
                    {
                        if (string.Compare(new DirectoryInfo(folder).Name, new DirectoryInfo(cutOutFolder).Name) == 0 &&
                            MessageBox.Show(
                                "Replace an existing folder?",
                                "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Directory.Delete(Path.Combine(
                                _view.MyTreeView.SelectedNode.Tag.ToString(), new DirectoryInfo(cutOutFolder).Name));

                            Directory.Move(
                                cutOutFolder,
                                Path.Combine(_view.MyTreeView.SelectedNode.Tag.ToString(),
                                    new DirectoryInfo(cutOutFolder).Name));

                            GetAllNodesForTheTreeNode(_model.ParentNode);

                            didItWork = false;
                            break;
                        }
                    }
                }

                if (didItWork && Directory.Exists(cutOutFolder))
                {
                    Directory.Move(
                        cutOutFolder,
                        Path.Combine(_view.MyTreeView.SelectedNode.Tag.ToString(),
                        new DirectoryInfo(cutOutFolder).Name));

                    GetAllNodesForTheTreeNode(_model.ParentNode);
                }
            }

            string[] files = Directory.GetFiles(_view.MyTreeView.SelectedNode.Tag.ToString());

            foreach (var cutOutFile in _model.PathForCut)
            {
                bool didItWork = true;

                foreach (var file in files)
                {
                    if (File.Exists(cutOutFile))
                    {
                        if (string.Compare(Path.GetFileName(file), Path.GetFileName(cutOutFile)) == 0 &&
                            MessageBox.Show(
                                "Replace an existing file?",
                                "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            File.Delete(
                                Path.Combine(
                                    _view.MyTreeView.SelectedNode.Tag.ToString(), Path.GetFileName(cutOutFile)));

                            File.Move(
                                cutOutFile,
                                Path.Combine(
                                    _view.MyTreeView.SelectedNode.Tag.ToString(), Path.GetFileName(cutOutFile)));

                            GetAllNodesForTheTreeNode(_model.ParentNode);
                            didItWork = false;
                            break;
                        }
                    }
                }

                if (didItWork && File.Exists(cutOutFile))
                {
                    File.Move(
                        cutOutFile,
                        Path.Combine(
                            _view.MyTreeView.SelectedNode.Tag.ToString(), Path.GetFileName(cutOutFile)));

                    GetAllNodesForTheTreeNode(_model.ParentNode);
                }
            }
        }
    }
}