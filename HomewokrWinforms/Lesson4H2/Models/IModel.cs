﻿using NLog;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Lesson4H2.Models
{
    public interface IModel
    {
        TreeNode ParentNode { get; set; }
        List<string> PathForCopy { get; set; }
        List<string> PathForCut { get; set; }
        Logger Logger { get; set; }
    }
}
