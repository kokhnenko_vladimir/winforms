﻿using NLog;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Lesson4H2.Models
{
    public class Model : IModel
    {
        public TreeNode ParentNode { get; set; } = new TreeNode();
        public List<string> PathForCopy { get; set; } = new List<string>();
        public List<string> PathForCut { get; set; } = new List<string>();
        public Logger Logger { get; set; } = LogManager.GetCurrentClassLogger();
    }
}
