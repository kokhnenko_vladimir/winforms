﻿using Lesson4H2.Views;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Lesson4H2
{
    public partial class MainFormFileExplorer : Form, IMainFormFileExplorer
    {
        public ListView MyListView => listView;
        public TreeView MyTreeView => treeView;
        public ToolStripButton ToolBarButtonOpen => toolBarButtonOpen;
        public ToolStripButton ToolBarButtonCut => toolBarButtonCut;
        public ToolStripButton ToolBarButtonCopy => toolBarButtonCopy;
        public ToolStripButton ToolBarButtonPaste => toolBarButtonPaste;
        public ToolStripButton ToolBarButtonDelete => toolBarButtonDelete;
        public ToolStripMenuItem MenuOpen => menuOpen;
        public ToolStripMenuItem MenuExit => menuExit;
        public ToolStripMenuItem MenuCut => menuCut;
        public ToolStripMenuItem MenuCopy => menuCopy;
        public ToolStripMenuItem MenuPaste => menuPaste;
        public ToolStripMenuItem MenuDelete => menuDelete;
        public ToolStripMenuItem ContextMenuOpen => contextMenuOpen;
        public ToolStripMenuItem ContextMenuCut => contextMenuCut;
        public ToolStripMenuItem ContextMenuCopy => contextMenuCopy;
        public ToolStripMenuItem ContextMenuPaste => contextMenuPaste;
        public ToolStripMenuItem ContextMenuDelete => contextMenuDelete;

        public MainFormFileExplorer()
        {
            InitializeComponent();

            ContextMenuCopy.Enabled = false;
            ContextMenuCut.Enabled = false;
            ContextMenuDelete.Enabled = false;
            ContextMenuPaste.Enabled = false;
            ContextMenuOpen.Enabled = false;

            MenuCopy.Enabled = false;
            MenuCut.Enabled = false;
            MenuDelete.Enabled = false;
            MenuOpen.Enabled = false;
            MenuPaste.Enabled = false;

            ToolBarButtonCopy.Enabled = false;
            ToolBarButtonCut.Enabled = false;
            ToolBarButtonDelete.Enabled = false;
            ToolBarButtonOpen.Enabled = false;
            ToolBarButtonPaste.Enabled = false;
        }

        public event EventHandler ListViewSelectedChange;
        public event EventHandler MainFormFileExplorerLoadEvent;
        public event EventHandler DeleteFileOrFolderEvent;
        public event TreeViewCancelEventHandler TreeViewBeforeExpandEvent;
        public event TreeNodeMouseClickEventHandler TreeViewNodeClickEvent;
        public event TreeViewEventHandler TreeViewAfterSelectEvent;
        public event KeyEventHandler TreeViewKeyDownEvent;
        public event KeyEventHandler ListViewKeyDownEvent;
        public event MouseEventHandler ListViewMouseDoubleClickEvent;
        public event EventHandler MenuOpenClickEvent;
        public event EventHandler ToolBarButtonOpenClickEvent;
        public event EventHandler ContextMenuOpenClickEvent;
        public event EventHandler ToolBarButtonBackClickEvent;
        public event EventHandler GeneralCutClickEvent;
        public event EventHandler GeneralPasteClickEvent;
        public event EventHandler GeneralCopyClickEvent;

        //
        // TreeView events
        //

        private void MainFormFileExplorer_Load(object sender, EventArgs e)
        {
            MainFormFileExplorerLoadEvent?.Invoke(this, EventArgs.Empty);
        }

        private void TreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeViewBeforeExpandEvent?.Invoke(this, e);
        }

        private void ContextMenuDelete_Click(object sender, EventArgs e)
        {
            DeleteFileOrFolderEvent?.Invoke(this, EventArgs.Empty);
        }

        private void MenuDelete_Click(object sender, EventArgs e)
        {
            DeleteFileOrFolderEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolBarButtonDelete_Click(object sender, EventArgs e)
        {
            DeleteFileOrFolderEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ContextMenuStripForListView_Opening(object sender, CancelEventArgs e)
        {
            ListViewSelectedChange?.Invoke(this, EventArgs.Empty);
        }

        private void MenuStrip_MenuActivate(object sender, EventArgs e)
        {
            ListViewSelectedChange?.Invoke(this, EventArgs.Empty);
        }

        private void ListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListViewSelectedChange?.Invoke(this, EventArgs.Empty);
        }

        private void TreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewNodeClickEvent?.Invoke(this, e);
        }

        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeViewAfterSelectEvent?.Invoke(this, e);
        }

        private void TreeView_KeyDown(object sender, KeyEventArgs e)
        {
            TreeViewKeyDownEvent?.Invoke(this, e);
        }

        private void ListView_KeyDown(object sender, KeyEventArgs e)
        {
            ListViewKeyDownEvent?.Invoke(this, e);
        }

        private void ListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewMouseDoubleClickEvent?.Invoke(this, e);
        }

        private void MenuOpen_Click(object sender, EventArgs e)
        {
            MenuOpenClickEvent?.Invoke(this, e);
        }

        private void ToolBarButtonOpen_Click(object sender, EventArgs e)
        {
            ToolBarButtonOpenClickEvent?.Invoke(this, e);
        }

        private void ContextMenuOpen_Click(object sender, EventArgs e)
        {
            ContextMenuOpenClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolBarButtonBack_Click(object sender, EventArgs e)
        {
            ToolBarButtonBackClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolBarButtonCut_Click(object sender, EventArgs e)
        {
            GeneralCutClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void MenuCut_Click(object sender, EventArgs e)
        {
            GeneralCutClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ContextMenuCut_Click(object sender, EventArgs e)
        {
            GeneralCutClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ContextMenuPaste_Click(object sender, EventArgs e)
        {
            GeneralPasteClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void MenuPaste_Click(object sender, EventArgs e)
        {
            GeneralPasteClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolBarButtonPaste_Click(object sender, EventArgs e)
        {
            GeneralPasteClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ToolBarButtonCopy_Click(object sender, EventArgs e)
        {
            GeneralCopyClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ContextMenuCopy_Click(object sender, EventArgs e)
        {
            GeneralCopyClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void MenuCopy_Click(object sender, EventArgs e)
        {
            GeneralCopyClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void MenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}