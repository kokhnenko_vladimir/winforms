﻿namespace Lesson4H2
{
    partial class MainFormFileExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormFileExplorer));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.listView = new System.Windows.Forms.ListView();
            this.columnHeaderText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLastModified = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStripForListView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView = new System.Windows.Forms.TreeView();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolBarButtonBack = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBarButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolBarButtonCut = new System.Windows.Forms.ToolStripButton();
            this.toolBarButtonCopy = new System.Windows.Forms.ToolStripButton();
            this.toolBarButtonPaste = new System.Windows.Forms.ToolStripButton();
            this.toolBarButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolBarTextBoxPath = new System.Windows.Forms.ToolStripTextBox();
            this.toolBarButtonPath = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBarTextBoxFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolBarButtonFind = new System.Windows.Forms.ToolStripButton();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripForListView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "harddrive-windows_97114.ico");
            this.imageList.Images.SetKeyName(1, "62917openfilefolder_109270 (1).ico");
            this.imageList.Images.SetKeyName(2, "fileinterfacesymboloftextpapersheet_79740.ico");
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderText,
            this.columnHeaderType,
            this.columnHeaderLastModified});
            this.listView.ContextMenuStrip = this.contextMenuStripForListView;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(740, 673);
            this.listView.SmallImageList = this.imageList;
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.ListView_ItemSelectionChanged);
            this.listView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListView_KeyDown);
            this.listView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListView_MouseDoubleClick);
            // 
            // columnHeaderText
            // 
            this.columnHeaderText.Text = "Text";
            this.columnHeaderText.Width = 475;
            // 
            // columnHeaderType
            // 
            this.columnHeaderType.Text = "Type";
            this.columnHeaderType.Width = 100;
            // 
            // columnHeaderLastModified
            // 
            this.columnHeaderLastModified.Text = "Last Modified";
            this.columnHeaderLastModified.Width = 100;
            // 
            // contextMenuStripForListView
            // 
            this.contextMenuStripForListView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenuOpen,
            this.contextMenuCut,
            this.contextMenuCopy,
            this.contextMenuPaste,
            this.contextMenuDelete});
            this.contextMenuStripForListView.Name = "contextMenuStripForListView";
            this.contextMenuStripForListView.Size = new System.Drawing.Size(147, 114);
            this.contextMenuStripForListView.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStripForListView_Opening);
            // 
            // contextMenuOpen
            // 
            this.contextMenuOpen.Name = "contextMenuOpen";
            this.contextMenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.contextMenuOpen.Size = new System.Drawing.Size(146, 22);
            this.contextMenuOpen.Text = "&Open";
            this.contextMenuOpen.Click += new System.EventHandler(this.ContextMenuOpen_Click);
            // 
            // contextMenuCut
            // 
            this.contextMenuCut.Name = "contextMenuCut";
            this.contextMenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.contextMenuCut.Size = new System.Drawing.Size(146, 22);
            this.contextMenuCut.Text = "C&ut";
            this.contextMenuCut.Click += new System.EventHandler(this.ContextMenuCut_Click);
            // 
            // contextMenuCopy
            // 
            this.contextMenuCopy.Name = "contextMenuCopy";
            this.contextMenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.contextMenuCopy.Size = new System.Drawing.Size(146, 22);
            this.contextMenuCopy.Text = "&Copy";
            this.contextMenuCopy.Click += new System.EventHandler(this.ContextMenuCopy_Click);
            // 
            // contextMenuPaste
            // 
            this.contextMenuPaste.Name = "contextMenuPaste";
            this.contextMenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.contextMenuPaste.Size = new System.Drawing.Size(146, 22);
            this.contextMenuPaste.Text = "&Paste";
            this.contextMenuPaste.Click += new System.EventHandler(this.ContextMenuPaste_Click);
            // 
            // contextMenuDelete
            // 
            this.contextMenuDelete.Name = "contextMenuDelete";
            this.contextMenuDelete.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.contextMenuDelete.Size = new System.Drawing.Size(146, 22);
            this.contextMenuDelete.Text = "&Delete";
            this.contextMenuDelete.Click += new System.EventHandler(this.ContextMenuDelete_Click);
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageList;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(370, 673);
            this.treeView.TabIndex = 0;
            this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.TreeView_BeforeExpand);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView_AfterSelect);
            this.treeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView_NodeMouseClick);
            this.treeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TreeView_KeyDown);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 52);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.listView);
            this.splitContainer.Size = new System.Drawing.Size(1114, 673);
            this.splitContainer.SplitterDistance = 370;
            this.splitContainer.TabIndex = 0;
            this.splitContainer.TabStop = false;
            // 
            // toolStrip
            // 
            this.toolStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarButtonBack,
            this.toolStripSeparator5,
            this.toolBarButtonOpen,
            this.toolStripSeparator,
            this.toolBarButtonCut,
            this.toolBarButtonCopy,
            this.toolBarButtonPaste,
            this.toolBarButtonDelete,
            this.toolBarTextBoxPath,
            this.toolBarButtonPath,
            this.toolStripSeparator3,
            this.toolStripSeparator2,
            this.toolBarTextBoxFind,
            this.toolBarButtonFind});
            this.toolStrip.Location = new System.Drawing.Point(4, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(211, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolBarButtonBack
            // 
            this.toolBarButtonBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("toolBarButtonBack.Image")));
            this.toolBarButtonBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonBack.Name = "toolBarButtonBack";
            this.toolBarButtonBack.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonBack.Text = "Back";
            this.toolBarButtonBack.Click += new System.EventHandler(this.ToolBarButtonBack_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolBarButtonOpen
            // 
            this.toolBarButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolBarButtonOpen.Image")));
            this.toolBarButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonOpen.Name = "toolBarButtonOpen";
            this.toolBarButtonOpen.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonOpen.Text = "&Open";
            this.toolBarButtonOpen.Click += new System.EventHandler(this.ToolBarButtonOpen_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // toolBarButtonCut
            // 
            this.toolBarButtonCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarButtonCut.Image = ((System.Drawing.Image)(resources.GetObject("toolBarButtonCut.Image")));
            this.toolBarButtonCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonCut.Name = "toolBarButtonCut";
            this.toolBarButtonCut.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonCut.Text = "C&ut";
            this.toolBarButtonCut.Click += new System.EventHandler(this.ToolBarButtonCut_Click);
            // 
            // toolBarButtonCopy
            // 
            this.toolBarButtonCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarButtonCopy.Image = ((System.Drawing.Image)(resources.GetObject("toolBarButtonCopy.Image")));
            this.toolBarButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonCopy.Name = "toolBarButtonCopy";
            this.toolBarButtonCopy.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonCopy.Text = "&Copy";
            this.toolBarButtonCopy.Click += new System.EventHandler(this.ToolBarButtonCopy_Click);
            // 
            // toolBarButtonPaste
            // 
            this.toolBarButtonPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarButtonPaste.Image = ((System.Drawing.Image)(resources.GetObject("toolBarButtonPaste.Image")));
            this.toolBarButtonPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonPaste.Name = "toolBarButtonPaste";
            this.toolBarButtonPaste.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonPaste.Text = "&Paste";
            this.toolBarButtonPaste.Click += new System.EventHandler(this.ToolBarButtonPaste_Click);
            // 
            // toolBarButtonDelete
            // 
            this.toolBarButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolBarButtonDelete.Image")));
            this.toolBarButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonDelete.Name = "toolBarButtonDelete";
            this.toolBarButtonDelete.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonDelete.Text = "Delete";
            this.toolBarButtonDelete.Click += new System.EventHandler(this.ToolBarButtonDelete_Click);
            // 
            // toolBarTextBoxPath
            // 
            this.toolBarTextBoxPath.Name = "toolBarTextBoxPath";
            this.toolBarTextBoxPath.Size = new System.Drawing.Size(100, 23);
            // 
            // toolBarButtonPath
            // 
            this.toolBarButtonPath.Name = "toolBarButtonPath";
            this.toolBarButtonPath.Size = new System.Drawing.Size(23, 4);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolBarTextBoxFind
            // 
            this.toolBarTextBoxFind.Name = "toolBarTextBoxFind";
            this.toolBarTextBoxFind.Size = new System.Drawing.Size(100, 23);
            // 
            // toolBarButtonFind
            // 
            this.toolBarButtonFind.Name = "toolBarButtonFind";
            this.toolBarButtonFind.Size = new System.Drawing.Size(23, 4);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.toolStripMenuItemEdit});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1114, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip";
            this.menuStrip.MenuActivate += new System.EventHandler(this.MenuStrip_MenuActivate);
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOpen,
            this.menuExit,
            this.toolStripSeparator4});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 20);
            this.menuFile.Text = "File";
            // 
            // menuOpen
            // 
            this.menuOpen.Name = "menuOpen";
            this.menuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuOpen.Size = new System.Drawing.Size(146, 22);
            this.menuOpen.Text = "&Open";
            this.menuOpen.Click += new System.EventHandler(this.MenuOpen_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.menuExit.Size = new System.Drawing.Size(146, 22);
            this.menuExit.Text = "&Exit";
            this.menuExit.Click += new System.EventHandler(this.MenuExit_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(143, 6);
            // 
            // toolStripMenuItemEdit
            // 
            this.toolStripMenuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCut,
            this.menuCopy,
            this.menuPaste,
            this.menuDelete});
            this.toolStripMenuItemEdit.Name = "toolStripMenuItemEdit";
            this.toolStripMenuItemEdit.Size = new System.Drawing.Size(39, 20);
            this.toolStripMenuItemEdit.Text = "Edit";
            // 
            // menuCut
            // 
            this.menuCut.Name = "menuCut";
            this.menuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.menuCut.Size = new System.Drawing.Size(144, 22);
            this.menuCut.Text = "C&ut";
            this.menuCut.Click += new System.EventHandler(this.MenuCut_Click);
            // 
            // menuCopy
            // 
            this.menuCopy.Name = "menuCopy";
            this.menuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuCopy.Size = new System.Drawing.Size(144, 22);
            this.menuCopy.Text = "&Copy";
            this.menuCopy.Click += new System.EventHandler(this.MenuCopy_Click);
            // 
            // menuPaste
            // 
            this.menuPaste.Name = "menuPaste";
            this.menuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuPaste.Size = new System.Drawing.Size(144, 22);
            this.menuPaste.Text = "&Paste";
            this.menuPaste.Click += new System.EventHandler(this.MenuPaste_Click);
            // 
            // menuDelete
            // 
            this.menuDelete.Name = "menuDelete";
            this.menuDelete.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.menuDelete.Size = new System.Drawing.Size(144, 22);
            this.menuDelete.Text = "&Delete";
            this.menuDelete.Click += new System.EventHandler(this.MenuDelete_Click);
            // 
            // MainFormFileExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 725);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.splitContainer);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainFormFileExplorer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Explorer";
            this.Load += new System.EventHandler(this.MainFormFileExplorer_Load);
            this.contextMenuStripForListView.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader columnHeaderText;
        private System.Windows.Forms.ColumnHeader columnHeaderType;
        private System.Windows.Forms.ColumnHeader columnHeaderLastModified;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolBarButtonOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton toolBarButtonCut;
        private System.Windows.Forms.ToolStripButton toolBarButtonCopy;
        private System.Windows.Forms.ToolStripButton toolBarButtonPaste;
        private System.Windows.Forms.ToolStripButton toolBarButtonDelete;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuOpen;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem menuCut;
        private System.Windows.Forms.ToolStripMenuItem menuCopy;
        private System.Windows.Forms.ToolStripMenuItem menuPaste;
        private System.Windows.Forms.ToolStripMenuItem menuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripForListView;
        private System.Windows.Forms.ToolStripMenuItem contextMenuCut;
        private System.Windows.Forms.ToolStripMenuItem contextMenuCopy;
        private System.Windows.Forms.ToolStripMenuItem contextMenuPaste;
        private System.Windows.Forms.ToolStripMenuItem contextMenuDelete;
        private System.Windows.Forms.ToolStripMenuItem contextMenuOpen;
        private System.Windows.Forms.ToolStripButton toolBarButtonBack;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripTextBox toolBarTextBoxPath;
        private System.Windows.Forms.ToolStripButton toolBarButtonPath;
        private System.Windows.Forms.ToolStripTextBox toolBarTextBoxFind;
        private System.Windows.Forms.ToolStripButton toolBarButtonFind;
    }
}

