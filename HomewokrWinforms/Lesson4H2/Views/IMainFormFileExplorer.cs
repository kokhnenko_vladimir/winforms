﻿using System;
using System.Windows.Forms;

namespace Lesson4H2.Views
{
    public interface IMainFormFileExplorer
    {
        ListView MyListView { get; }
        TreeView MyTreeView { get; }

        ToolStripButton ToolBarButtonOpen { get; }
        ToolStripButton ToolBarButtonCut { get; }
        ToolStripButton ToolBarButtonCopy { get; }
        ToolStripButton ToolBarButtonPaste { get; }
        ToolStripButton ToolBarButtonDelete { get; }

        ToolStripMenuItem MenuOpen { get; }
        ToolStripMenuItem MenuExit { get; }
        ToolStripMenuItem MenuCut { get; }
        ToolStripMenuItem MenuCopy { get; }
        ToolStripMenuItem MenuPaste { get; }
        ToolStripMenuItem MenuDelete { get; }

        ToolStripMenuItem ContextMenuOpen { get; }
        ToolStripMenuItem ContextMenuCut { get; }
        ToolStripMenuItem ContextMenuCopy { get; }
        ToolStripMenuItem ContextMenuPaste { get; }
        ToolStripMenuItem ContextMenuDelete { get; }

        event EventHandler ListViewSelectedChange;
        event EventHandler MainFormFileExplorerLoadEvent;
        event EventHandler DeleteFileOrFolderEvent;
        event TreeViewCancelEventHandler TreeViewBeforeExpandEvent;
        event TreeNodeMouseClickEventHandler TreeViewNodeClickEvent;
        event TreeViewEventHandler TreeViewAfterSelectEvent;
        event KeyEventHandler TreeViewKeyDownEvent;
        event KeyEventHandler ListViewKeyDownEvent;
        event MouseEventHandler ListViewMouseDoubleClickEvent;
        event EventHandler MenuOpenClickEvent;
        event EventHandler ToolBarButtonOpenClickEvent;
        event EventHandler ContextMenuOpenClickEvent;
        event EventHandler ToolBarButtonBackClickEvent;
        event EventHandler GeneralCutClickEvent;
        event EventHandler GeneralCopyClickEvent;
        event EventHandler GeneralPasteClickEvent;
    }
}