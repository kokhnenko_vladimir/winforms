﻿using Lesson4H2.Models;
using Lesson4H2.Presenters;
using System;
using System.Windows.Forms;

namespace Lesson4H2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var form = new MainFormFileExplorer();
            new FormFileExplorerPresenter(form, new Model());

            Application.Run(form);
        }
    }
}
