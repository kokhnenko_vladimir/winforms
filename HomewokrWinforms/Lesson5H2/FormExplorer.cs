﻿using Lesson5H2.ListViewsIerarhy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Lesson5H2
{
    public partial class FormExplorer : Form
    {
        private readonly Stack<string> _root = new Stack<string>();
        private ListViewBase _curView = new ListViewDetail();

        public FormExplorer()
        {
            InitializeComponent();
            listView.LargeImageList = imageListDetails;
            listView.View = _curView.ListView;
        }

        private void FormExplorer_Load(object sender, EventArgs e)
        {
            _curView.UpdateListViewHDD(listView);
        }

        private void ListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView.SelectedItems.Count == 1)
            {
                _curView.UpdateFilesAndFoldersForward(listView, _root);
            }
        }

        private void ListView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                if (_root.Count > 0)
                {
                    _curView.UpdateFilesAndFoldersReverce(listView, _root);
                }
                else if (new DirectoryInfo(listView.Items[0].Tag.ToString()).Exists)
                {
                    _curView.UpdateListViewHDD(listView);
                }
            }
            else if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (listView.SelectedItems.Count == 1)
                {
                    _curView.UpdateFilesAndFoldersForward(listView, _root);
                }
            }
        }

        private void ToolStripDetail_Click(object sender, EventArgs e)
        {
            _root.Clear();
            _curView = new ListViewDetail();
            listView.SmallImageList = imageListDetails;
            listView.View = _curView.ListView;
            _curView.UpdateListViewHDD(listView);
        }

        private void ToolStripLargeIcon_Click(object sender, EventArgs e)
        {
            _root.Clear();
            _curView = new ListViewLargeIcons();
            listView.LargeImageList = imageListLargeIcons;
            listView.View = _curView.ListView;
            _curView.UpdateListViewHDD(listView);
        }

        private void ToolStripList_Click(object sender, EventArgs e)
        {
            _root.Clear();
            _curView = new ListViewList();
            listView.SmallImageList = imageListDetails;
            listView.View = _curView.ListView;
            _curView.UpdateListViewHDD(listView);
        }

        private void ToolStripTile_Click(object sender, EventArgs e)
        {
            _root.Clear();
            _curView = new ListViewTile();
            listView.LargeImageList = imageListTile;
            listView.View = _curView.ListView;
            _curView.UpdateListViewHDD(listView);
        }
    }
}