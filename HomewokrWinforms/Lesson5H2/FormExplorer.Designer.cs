﻿namespace Lesson5H2
{
    partial class FormExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormExplorer));
            this.listView = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageListDetails = new System.Windows.Forms.ImageList(this.components);
            this.imageListTile = new System.Windows.Forms.ImageList(this.components);
            this.imageListLargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStripListView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLargeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTile = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripListView.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colDate,
            this.colType,
            this.colSize});
            this.listView.ContextMenuStrip = this.contextMenuStripListView;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(800, 450);
            this.listView.SmallImageList = this.imageListDetails;
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListView_KeyDown);
            this.listView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ListView_MouseDoubleClick);
            // 
            // colName
            // 
            this.colName.Text = "Имя";
            this.colName.Width = 209;
            // 
            // colDate
            // 
            this.colDate.Text = "Дата";
            this.colDate.Width = 134;
            // 
            // colType
            // 
            this.colType.Text = "Тип";
            this.colType.Width = 90;
            // 
            // colSize
            // 
            this.colSize.Text = "Размер";
            this.colSize.Width = 142;
            // 
            // imageListDetails
            // 
            this.imageListDetails.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListDetails.ImageStream")));
            this.imageListDetails.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListDetails.Images.SetKeyName(0, "harddrive_Details.ico");
            this.imageListDetails.Images.SetKeyName(1, "imageListFolder_Details.ico");
            this.imageListDetails.Images.SetKeyName(2, "imageListFile_Details.ico");
            // 
            // imageListTile
            // 
            this.imageListTile.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTile.ImageStream")));
            this.imageListTile.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTile.Images.SetKeyName(0, "drive_Tile.ico");
            this.imageListTile.Images.SetKeyName(1, "folder_Tile.ico");
            this.imageListTile.Images.SetKeyName(2, "file_Tile.ico");
            // 
            // imageListLargeIcons
            // 
            this.imageListLargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLargeIcons.ImageStream")));
            this.imageListLargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLargeIcons.Images.SetKeyName(0, "harddisk_LargeIcon.ico");
            this.imageListLargeIcons.Images.SetKeyName(1, "folder_LargeIcon.ico");
            this.imageListLargeIcons.Images.SetKeyName(2, "file_LargeIcon.ico");
            // 
            // contextMenuStripListView
            // 
            this.contextMenuStripListView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDetail,
            this.toolStripLargeIcon,
            this.toolStripList,
            this.toolStripTile});
            this.contextMenuStripListView.Name = "contextMenuStripListView";
            this.contextMenuStripListView.Size = new System.Drawing.Size(121, 92);
            // 
            // toolStripDetail
            // 
            this.toolStripDetail.Name = "toolStripDetail";
            this.toolStripDetail.Size = new System.Drawing.Size(180, 22);
            this.toolStripDetail.Text = "Таблица";
            this.toolStripDetail.Click += new System.EventHandler(this.ToolStripDetail_Click);
            // 
            // toolStripLargeIcon
            // 
            this.toolStripLargeIcon.Name = "toolStripLargeIcon";
            this.toolStripLargeIcon.Size = new System.Drawing.Size(180, 22);
            this.toolStripLargeIcon.Text = "Иконки";
            this.toolStripLargeIcon.Click += new System.EventHandler(this.ToolStripLargeIcon_Click);
            // 
            // toolStripList
            // 
            this.toolStripList.Name = "toolStripList";
            this.toolStripList.Size = new System.Drawing.Size(180, 22);
            this.toolStripList.Text = "Список";
            this.toolStripList.Click += new System.EventHandler(this.ToolStripList_Click);
            // 
            // toolStripTile
            // 
            this.toolStripTile.Name = "toolStripTile";
            this.toolStripTile.Size = new System.Drawing.Size(180, 22);
            this.toolStripTile.Text = "Плитка";
            this.toolStripTile.Click += new System.EventHandler(this.ToolStripTile_Click);
            // 
            // FormExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listView);
            this.Name = "FormExplorer";
            this.Text = "Проводник";
            this.Load += new System.EventHandler(this.FormExplorer_Load);
            this.contextMenuStripListView.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ImageList imageListDetails;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colDate;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colSize;
        private System.Windows.Forms.ImageList imageListTile;
        private System.Windows.Forms.ImageList imageListLargeIcons;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripListView;
        private System.Windows.Forms.ToolStripMenuItem toolStripDetail;
        private System.Windows.Forms.ToolStripMenuItem toolStripLargeIcon;
        private System.Windows.Forms.ToolStripMenuItem toolStripList;
        private System.Windows.Forms.ToolStripMenuItem toolStripTile;
    }
}

