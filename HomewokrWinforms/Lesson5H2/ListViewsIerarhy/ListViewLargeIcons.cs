﻿using System.Windows.Forms;

namespace Lesson5H2.ListViewsIerarhy
{
    class ListViewLargeIcons : ListViewBase
    {
        public override View ListView => View.LargeIcon;
    }
}
