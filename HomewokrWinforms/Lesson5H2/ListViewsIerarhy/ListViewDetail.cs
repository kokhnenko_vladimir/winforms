﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace Lesson5H2.ListViewsIerarhy
{
    class ListViewDetail : ListViewBase
    {
        public override View ListView => View.Details;

        public override void UpdateFilesAndFoldersForward(ListView listView, Stack<string> root)
        {
            try
            {
                string path = listView.SelectedItems[0].Tag.ToString();

                var tempDir = new DirectoryInfo(path);
                if (!string.IsNullOrEmpty(tempDir.Parent?.FullName))
                {
                    root.Push(tempDir.Parent.FullName);
                }

                ListViewSubItem[] subItems;
                listView.Items.Clear();

                if (new DirectoryInfo(path).Exists)
                {
                    foreach (var dir in Directory.GetDirectories(path))
                    {
                        var curDir = new DirectoryInfo(dir);
                        var item = new ListViewItem(curDir.Name, 1) { Tag = curDir.FullName };

                        subItems = new ListViewItem.ListViewSubItem[]
                        {
                            new ListViewItem.ListViewSubItem(item, curDir.CreationTime.ToString()),
                            new ListViewItem.ListViewSubItem(item, "Папка"),
                            new ListViewItem.ListViewSubItem(item, string.Empty)
                        };

                        item.SubItems.AddRange(subItems);
                        listView.Items.Add(item);
                    }

                    foreach (var file in Directory.GetFiles(path))
                    {
                        var fileInfo = new FileInfo(file);
                        var item = new ListViewItem(fileInfo.Name, 2) { Tag = fileInfo.FullName };

                        subItems = new ListViewItem.ListViewSubItem[]
                        {
                            new ListViewItem.ListViewSubItem(item, fileInfo.CreationTime.ToString()),
                            new ListViewItem.ListViewSubItem(item, "Папка"),
                            new ListViewItem.ListViewSubItem(item, $"{fileInfo.Length / 1_000_000} Мб")
                        };

                        item.SubItems.AddRange(subItems);
                        listView.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public override void UpdateFilesAndFoldersReverce(ListView listView, Stack<string> root)
        {
            try
            {
                string path = root.Pop();

                ListViewSubItem[] subItems;
                listView.Items.Clear();

                if (new DirectoryInfo(path).Exists)
                {
                    foreach (var dir in Directory.GetDirectories(path))
                    {
                        var curDir = new DirectoryInfo(dir);
                        var item = new ListViewItem(curDir.Name, 1) { Tag = curDir.FullName };


                        subItems = new ListViewItem.ListViewSubItem[]
                        {
                            new ListViewItem.ListViewSubItem(item, curDir.CreationTime.ToString()),
                            new ListViewItem.ListViewSubItem(item, "Папка"),
                            new ListViewItem.ListViewSubItem(item, string.Empty)
                        };

                        item.SubItems.AddRange(subItems);
                        listView.Items.Add(item);
                    }

                    foreach (var file in Directory.GetFiles(path))
                    {
                        var fileInfo = new FileInfo(file);
                        var item = new ListViewItem(fileInfo.Name, 2) { Tag = fileInfo.FullName };

                        subItems = new ListViewItem.ListViewSubItem[]
                        {
                            new ListViewItem.ListViewSubItem(item, fileInfo.CreationTime.ToString()),
                            new ListViewItem.ListViewSubItem(item, "Папка"),
                            new ListViewItem.ListViewSubItem(item, $"{fileInfo.Length / 1_000_000} Мб")
                        };

                        item.SubItems.AddRange(subItems);
                        listView.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public override void UpdateListViewHDD(ListView listView)
        {
            try
            {
                DriveInfo[] info = DriveInfo.GetDrives();
                ListViewSubItem[] subItems;
                listView.Items.Clear();

                for (int i = 0; i < info.Length; i++)
                {
                    var item = new ListViewItem(info[i].Name, 0) { Tag = info[i].Name };

                    subItems = new ListViewItem.ListViewSubItem[]
                    {
                        new ListViewItem.ListViewSubItem(item, string.Empty),
                        new ListViewItem.ListViewSubItem(item, info[i].VolumeLabel.ToString()),
                        new ListViewItem.ListViewSubItem(item, $"{info[i].TotalSize / 1000_000_000} Гб")
                    };
                    item.SubItems.AddRange(subItems);
                    listView.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}