﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Lesson5H2.ListViewsIerarhy
{
    abstract class ListViewBase
    {
        public abstract View ListView { get; }

        public virtual void UpdateFilesAndFoldersForward(ListView listView, Stack<string> root)
        {
            try
            {
                string path = listView.SelectedItems[0].Tag.ToString();

                var tempDir = new DirectoryInfo(path);
                if (!string.IsNullOrEmpty(tempDir.Parent?.FullName))
                {
                    root.Push(tempDir.Parent.FullName);
                }

                listView.Items.Clear();

                if (new DirectoryInfo(path).Exists)
                {
                    foreach (var dir in Directory.GetDirectories(path))
                    {
                        listView.Items.Add(
                            new ListViewItem(new DirectoryInfo(dir).Name, 1) { Tag = new DirectoryInfo(dir).FullName });
                    }

                    foreach (var file in Directory.GetFiles(path))
                    {
                        listView.Items.Add(
                            new ListViewItem(new FileInfo(file).Name, 2) { Tag = new FileInfo(file).FullName });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public virtual void UpdateFilesAndFoldersReverce(ListView listView, Stack<string> root)
        {
            try
            {
                string path = root.Pop();
                listView.Items.Clear();

                if (new DirectoryInfo(path).Exists)
                {
                    foreach (var dir in Directory.GetDirectories(path))
                    {
                        listView.Items.Add(
                            new ListViewItem(new DirectoryInfo(dir).Name, 1) { Tag = new DirectoryInfo(dir).FullName });
                    }

                    foreach (var file in Directory.GetFiles(path))
                    {
                        listView.Items.Add(
                            new ListViewItem(new FileInfo(file).Name, 2) { Tag = new FileInfo(file).FullName });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public virtual void UpdateListViewHDD(ListView listView)
        {
            try
            {
                DriveInfo[] info = DriveInfo.GetDrives();
                listView.Items.Clear();

                for (int i = 0; i < info.Length; i++)
                {
                    listView.Items.Add(
                        new ListViewItem(info[i].Name, 0) { Tag = info[i].Name });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}