﻿using Lesson6H2.FormPrimitives;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lesson6H2
{
    public partial class FormPrimitive : Form
    {
        private readonly List<IPaintIerarhy> _paint = new List<IPaintIerarhy>();
        private IPaintIerarhy _curPaint = new PointFigure();
        private int _x;
        private int _y;
        private int _width;
        private int _height;
        private Brush _curBrush = Brushes.Black;

        public FormPrimitive()
        {
            InitializeComponent();
        }

        private void FormPrimitive_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            foreach (var item in _paint)
            {
                item.DrawYourself(g);
            }
        }

        private void FormPrimitive_MouseDown(object sender, MouseEventArgs e)
        {
            _x = e.X;
            _y = e.Y;
        }

        private void FormPrimitive_MouseUp(object sender, MouseEventArgs e)
        {
            ChangeValues(e.X, e.Y);

            if (_curPaint is PointFigure)
                _paint?.Add(new PointFigure(_x, _y, _width, _height, _curBrush));
            else if (_curPaint is RectangleFigure)
                _paint?.Add(new RectangleFigure(_x, _y, _width, _height, _curBrush));
            else if (_curPaint is ElipseFigure)
                _paint?.Add(new ElipseFigure(_x, _y, _width, _height, _curBrush));

            Invalidate();
        }

        private void ToolStripPoint_Click(object sender, EventArgs e)
        {
            _curPaint = new PointFigure();
        }

        private void ToolStripRectangle_Click(object sender, EventArgs e)
        {
            _curPaint = new RectangleFigure();
        }

        private void ToolStripElipse_Click(object sender, EventArgs e)
        {
            _curPaint = new ElipseFigure();
        }

        private void ToolStripColor_Click(object sender, EventArgs e)
        {
            var colorDialog = new ColorDialog();

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                SolidBrush solidBrush = new SolidBrush(colorDialog.Color);
                _curBrush = solidBrush;
            }
        }

        private void ToolStripSave_Click(object sender, EventArgs e)
        {
            Bitmap bt = new Bitmap(this.Size.Width, this.Size.Height);
            Rectangle rect = new Rectangle(0, 0, this.Size.Width, this.Size.Height);
            this.DrawToBitmap(bt, rect);
            bt.Save("NewBitmap.png");
        }
        private void ChangeValues(int x, int y)
        {
            if (_x < x)
            {
                _width = x - _x;
            }
            else
            {
                int tmp = x;
                x = _x;
                _x = tmp;
                _width = x - _x;
            }

            if (_y < y)
            {
                _height = y - _y;
            }
            else
            {
                int tmp = y;
                y = _y;
                _y = tmp;
                _height = y - _y;
            }
        }
    }
}