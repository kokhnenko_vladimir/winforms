﻿using System.Drawing;

namespace Lesson6H2.FormPrimitives
{
    class PointFigure : IPaintIerarhy
    {
        public Brush BrushFigure { get; set; } = Brushes.Black;
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get => 5; set => value = 5; }
        public int Height { get => 5; set => value = 5; }

        public PointFigure() { }

        public PointFigure(int x, int y, int width, int height, Brush brush)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            BrushFigure = brush;
        }

        public void DrawYourself(Graphics g)
        {
            g.FillEllipse(BrushFigure, X, Y, Width, Height);
        }
    }
}