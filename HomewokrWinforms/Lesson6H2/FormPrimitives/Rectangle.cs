﻿using System.Drawing;

namespace Lesson6H2.FormPrimitives
{
    class RectangleFigure : IPaintIerarhy
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Brush BrushFigure { get; set; } = Brushes.Black;

        public RectangleFigure() { }

        public RectangleFigure(int x, int y, int width, int height, Brush brush)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            BrushFigure = brush;
        }

        public void DrawYourself(Graphics g)
        {
            g.FillRectangle(BrushFigure, X, Y, Width, Height);
        }
    }
}