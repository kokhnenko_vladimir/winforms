﻿using System.Drawing;

namespace Lesson6H2.FormPrimitives
{
    interface IPaintIerarhy
    {
        int X { get; set; }
        int Y { get; set; }
        int Width { get; set; }
        int Height { get; set; }

        Brush BrushFigure { get; set; }

        void DrawYourself(Graphics g);
    }
}