﻿namespace Lesson6H2
{
    partial class FormPrimitive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripPrimitive = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripPoint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripRectangle = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripElipse = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripColor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPrimitive,
            this.toolStripColor,
            this.toolStripSave});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1067, 30);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // toolStripPrimitive
            // 
            this.toolStripPrimitive.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPoint,
            this.toolStripRectangle,
            this.toolStripElipse});
            this.toolStripPrimitive.Name = "toolStripPrimitive";
            this.toolStripPrimitive.Size = new System.Drawing.Size(81, 26);
            this.toolStripPrimitive.Text = "Primitive";
            // 
            // toolStripPoint
            // 
            this.toolStripPoint.Name = "toolStripPoint";
            this.toolStripPoint.Size = new System.Drawing.Size(158, 26);
            this.toolStripPoint.Text = "Point";
            this.toolStripPoint.Click += new System.EventHandler(this.ToolStripPoint_Click);
            // 
            // toolStripRectangle
            // 
            this.toolStripRectangle.Name = "toolStripRectangle";
            this.toolStripRectangle.Size = new System.Drawing.Size(158, 26);
            this.toolStripRectangle.Text = "Rectangle";
            this.toolStripRectangle.Click += new System.EventHandler(this.ToolStripRectangle_Click);
            // 
            // toolStripElipse
            // 
            this.toolStripElipse.Name = "toolStripElipse";
            this.toolStripElipse.Size = new System.Drawing.Size(158, 26);
            this.toolStripElipse.Text = "Elipse";
            this.toolStripElipse.Click += new System.EventHandler(this.ToolStripElipse_Click);
            // 
            // toolStripColor
            // 
            this.toolStripColor.Name = "toolStripColor";
            this.toolStripColor.Size = new System.Drawing.Size(59, 26);
            this.toolStripColor.Text = "Color";
            this.toolStripColor.Click += new System.EventHandler(this.ToolStripColor_Click);
            // 
            // toolStripSave
            // 
            this.toolStripSave.Name = "toolStripSave";
            this.toolStripSave.Size = new System.Drawing.Size(54, 26);
            this.toolStripSave.Text = "Save";
            this.toolStripSave.Click += new System.EventHandler(this.ToolStripSave_Click);
            // 
            // FormPrimitive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormPrimitive";
            this.Text = "Draw Primitives";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormPrimitive_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormPrimitive_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormPrimitive_MouseUp);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripPrimitive;
        private System.Windows.Forms.ToolStripMenuItem toolStripPoint;
        private System.Windows.Forms.ToolStripMenuItem toolStripRectangle;
        private System.Windows.Forms.ToolStripMenuItem toolStripElipse;
        private System.Windows.Forms.ToolStripMenuItem toolStripColor;
        private System.Windows.Forms.ToolStripMenuItem toolStripSave;
    }
}

