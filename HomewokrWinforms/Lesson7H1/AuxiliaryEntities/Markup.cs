﻿namespace Lesson7H1.AuxiliaryEntities
{
    struct Markup
    {
        public int RightEdge { get; set; }
        public int BottomEdge { get; set; }
        public int CenterWidth { get; set; }
        public int CenterHeight { get; set; }
    }
}