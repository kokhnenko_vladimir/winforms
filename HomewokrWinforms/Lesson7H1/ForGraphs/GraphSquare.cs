﻿using Lesson7H1.AuxiliaryEntities;
using System.Drawing;

namespace Lesson7H1.ForGraphs
{
    class GraphSquare : IGraph
    {
        public void DrawYourself(Graphics g, Markup markup)
        {
            Point[] points = new Point[41];

            for (int i = 0, x = -20; i <= 40; i++, x++)
                points[i] = new Point(markup.CenterWidth + x, markup.CenterHeight - x * x);
            g.DrawCurve(new Pen(Brushes.Red, 2F), points, 1.0F);
        }
    }
}