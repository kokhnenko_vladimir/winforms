﻿using Lesson7H1.AuxiliaryEntities;
using System.Drawing;

namespace Lesson7H1.ForGraphs
{
    interface IGraph
    {
        void DrawYourself(Graphics g, Markup markup);
    }
}