﻿using Lesson7H1.AuxiliaryEntities;
using System.Drawing;

namespace Lesson7H1.ForGraphs
{
    class GraphLine : IGraph
    {
        public void DrawYourself(Graphics g, Markup markup)
        {
            const int x1 = -100;
            const int x2 = 100;

            Point start = new Point(markup.CenterWidth + x1, markup.CenterHeight - x1 * 2);
            Point end = new Point(markup.CenterWidth + x2, markup.CenterHeight - x2 * 2);

            g.DrawLine(new Pen(Brushes.Red, 2F), start, end);
        }
    }
}