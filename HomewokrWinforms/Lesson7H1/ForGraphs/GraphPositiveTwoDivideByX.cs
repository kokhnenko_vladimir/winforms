﻿using Lesson7H1.AuxiliaryEntities;
using System.Drawing;

namespace Lesson7H1.ForGraphs
{
    class GraphPositiveTwoDivideByX : IGraph
    {
        public void DrawYourself(Graphics g, Markup markup)
        {
            Point[] pointsPositive = new Point[100];
            Point[] pointsNegative = new Point[100];

            for (int i = 0, x = -100; i < 100; i++, x++)
            {
                pointsNegative[i] = new Point(markup.CenterWidth + x - 2, markup.CenterHeight - 100 / x + 2);
            }

            for (int i = 0, x = 1; i < 100; i++, x++)
            {
                pointsPositive[i] = new Point(markup.CenterWidth + x + 2, markup.CenterHeight - 100 / x - 2);
            }

            g.DrawCurve(new Pen(Brushes.Red, 2F), pointsNegative, 1.0F);
            g.DrawCurve(new Pen(Brushes.Red, 2F), pointsPositive, 1.0F);
        }
    }
}