﻿using Lesson7H1.AuxiliaryEntities;
using Lesson7H1.ForGraphs;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lesson7H1
{
    public partial class FormMathGraphs : Form
    {
        private readonly Markup _markup = new Markup();
        private IGraph _curGraph = new GraphPositiveTwoDivideByX();

        public FormMathGraphs()
        {
            InitializeComponent();

            _markup.RightEdge = this.Size.Width - 500;
            _markup.BottomEdge = this.Size.Height;
            _markup.CenterHeight = _markup.BottomEdge / 2;
            _markup.CenterWidth = _markup.RightEdge / 2;
        }

        private void FormMathGraphs_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            g.DrawLine(new Pen(Brushes.Blue), new Point(0, _markup.CenterHeight), new Point(_markup.RightEdge, _markup.CenterHeight));
            g.DrawLine(new Pen(Brushes.Blue), new Point(_markup.CenterWidth, 0), new Point(_markup.CenterWidth, _markup.BottomEdge));

            _curGraph.DrawYourself(g, _markup);
        }

        private void CheckedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox.Items.Count; i++)
            {
                checkedListBox.SetItemChecked(i, false);
            }
            checkedListBox.SetItemChecked(checkedListBox.SelectedIndex, true);

            switch (checkedListBox.SelectedIndex)
            {
                case 0:
                    _curGraph = new GraphLine();
                    break;
                case 1:
                    _curGraph = new GraphSquare();
                    break;
                case 2:
                    _curGraph = new GraphPositiveTwoDivideByX();
                    break;
            }

            Invalidate();
        }
    }
}