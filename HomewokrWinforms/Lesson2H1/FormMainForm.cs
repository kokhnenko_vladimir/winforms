﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Lesson2H1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Enter_Click(object sender, EventArgs e)
        {
            try
            {
                using (FileStream fin = File.OpenRead(textBoxInputPathFile.Text))
                {
                    int[] array = new int[fin.Length];
                    progressBar.Minimum = 0;
                    progressBar.Maximum = (int)fin.Length - 1;
                    progressBar.Step = 1;
                    progressBar.Value = 0;
                    for (int i = 0; i < fin.Length - 1; i++)
                    {
                        array[i] = fin.ReadByte();
                        progressBar.PerformStep();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
