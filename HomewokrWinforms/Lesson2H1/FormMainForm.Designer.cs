﻿namespace Lesson2H1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxProgressBar = new System.Windows.Forms.GroupBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.textBoxInputPathFile = new System.Windows.Forms.TextBox();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.groupBoxProgressBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxProgressBar
            // 
            this.groupBoxProgressBar.Controls.Add(this.progressBar);
            this.groupBoxProgressBar.Location = new System.Drawing.Point(34, 113);
            this.groupBoxProgressBar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxProgressBar.Name = "groupBoxProgressBar";
            this.groupBoxProgressBar.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBoxProgressBar.Size = new System.Drawing.Size(533, 81);
            this.groupBoxProgressBar.TabIndex = 0;
            this.groupBoxProgressBar.TabStop = false;
            this.groupBoxProgressBar.Text = "Прогресс считывания файла";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(18, 36);
            this.progressBar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(500, 19);
            this.progressBar.TabIndex = 0;
            // 
            // textBoxInputPathFile
            // 
            this.textBoxInputPathFile.Location = new System.Drawing.Point(52, 35);
            this.textBoxInputPathFile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxInputPathFile.Name = "textBoxInputPathFile";
            this.textBoxInputPathFile.Size = new System.Drawing.Size(500, 20);
            this.textBoxInputPathFile.TabIndex = 1;
            this.textBoxInputPathFile.Text = "C:\\Users\\vanko\\MyWorkFolder\\C#\\WinForms\\winforms\\Lesson2H1\\Chap14.cs";
            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(272, 227);
            this.buttonEnter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(56, 19);
            this.buttonEnter.TabIndex = 2;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.Enter_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this.buttonEnter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 271);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.textBoxInputPathFile);
            this.Controls.Add(this.groupBoxProgressBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Lessong2H1";
            this.groupBoxProgressBar.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxProgressBar;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox textBoxInputPathFile;
        private System.Windows.Forms.Button buttonEnter;
    }
}

