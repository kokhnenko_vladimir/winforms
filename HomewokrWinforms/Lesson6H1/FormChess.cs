﻿using System.Drawing;
using System.Windows.Forms;

namespace Lesson6H1
{
    public partial class FormChess : Form
    {
        private readonly Bitmap _blackPawn = new Bitmap(@"..\..\\Resources\blackPawn.png");
        private readonly Bitmap _blackRook = new Bitmap(@"..\..\\Resources\blackRook.png");
        private readonly Bitmap _blackKnight = new Bitmap(@"..\..\\Resources\blackKnight.png");
        private readonly Bitmap _blackBishop = new Bitmap(@"..\..\\Resources\blackBishop.png");
        private readonly Bitmap _blackQueen = new Bitmap(@"..\..\\Resources\blackQueen.png");
        private readonly Bitmap _blackKing = new Bitmap(@"..\..\\Resources\blackKing.png");

        private readonly Bitmap _whitePawn = new Bitmap(@"..\..\\Resources\whitePawn.png");
        private readonly Bitmap _whiteRook = new Bitmap(@"..\..\\Resources\whiteRook.png");
        private readonly Bitmap _whiteKnight = new Bitmap(@"..\..\\Resources\whiteKnight.png");
        private readonly Bitmap _whiteBishop = new Bitmap(@"..\..\\Resources\whiteBishop.png");
        private readonly Bitmap _whiteQueen = new Bitmap(@"..\..\\Resources\whiteQueen.png");
        private readonly Bitmap _whiteKing = new Bitmap(@"..\..\\Resources\whiteKing.png");

        public FormChess()
        {
            InitializeComponent();
        }

        private void FormChess_Paint(object sender, PaintEventArgs e)
        {
            const int widthDesk = 640;
            const int heightDesk = 640;

            Graphics g = e.Graphics;

            Point sPointMainRect = new Point(0, 0);
            Size sizeMainRect = new Size(640, 640);
            Rectangle mainRect = new Rectangle(sPointMainRect, sizeMainRect);

            g.FillRectangle(Brushes.White, mainRect);


            Size sizeBlackRectangle = new Size(80, 80);

            for (int i = 0, k = 0; i < heightDesk; i += 80, k++)
            {
                for (int j = 0, m = 0; j < widthDesk; j += 80, m++)
                {
                    if (m % 2 != 0 && k % 2 == 0)
                        g.FillRectangle(Brushes.Brown, new Rectangle(new Point(j, i), sizeBlackRectangle));
                    else if (m % 2 == 0 && k % 2 != 0)
                        g.FillRectangle(Brushes.Brown, new Rectangle(new Point(j, i), sizeBlackRectangle));

                    if (k == 0)
                    {
                        for (int n = 0, p = 0; n < widthDesk; n += 80, p++)
                        {
                            if (p == 0 || p == 7)
                                g.DrawImage(_blackRook, n, i, 80, 80);
                            else if (p == 1 || p == 6)
                                g.DrawImage(_blackKnight, n, i, 80, 80);
                            else if (p == 2 || p == 5)
                                g.DrawImage(_blackBishop, n, i, 80, 80);
                            else if (p == 3)
                                g.DrawImage(_blackQueen, n, i, 80, 80);
                            else if (p == 4)
                                g.DrawImage(_blackKing, n, i, 80, 80);
                        }
                    }
                    else if (k == 1)
                    {
                        for (int n = 0; n < widthDesk; n += 80)
                        {
                            g.DrawImage(_blackPawn, j, i, 80, 80);
                        }
                    }
                    else if (k == 6)
                    {
                        for (int n = 0; n < widthDesk; n += 80)
                        {
                            g.DrawImage(_whitePawn, j, i, 80, 80);
                        }
                    }
                    else if (k == 7)
                    {
                        for (int n = 0, p = 0; n < widthDesk; n += 80, p++)
                        {
                            if (p == 0 || p == 7)
                                g.DrawImage(_whiteRook, n, i, 80, 80);
                            else if (p == 1 || p == 6)
                                g.DrawImage(_whiteKnight, n, i, 80, 80);
                            else if (p == 2 || p == 5)
                                g.DrawImage(_whiteBishop, n, i, 80, 80);
                            else if (p == 3)
                                g.DrawImage(_whiteQueen, n, i, 80, 80);
                            else if (p == 4)
                                g.DrawImage(_whiteKing, n, i, 80, 80);
                        }
                    }
                }
            }
        }
    }
}