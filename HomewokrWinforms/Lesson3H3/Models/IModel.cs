﻿namespace Lesson3H3.Models
{
    public interface IModel
    {
        string Text { get; set; }
    }
}
