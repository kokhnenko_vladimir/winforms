﻿using Lesson3H3.Models;
using Lesson3H3.Views;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson3H3.Presenters
{
    public class PresenterMainForm
    {
        private readonly IMainView _view;
        private readonly IModel _model;

        public Form MainForm { get => (Form)_view; }

        public PresenterMainForm(IMainView mainForm)
        {
            _view = mainForm;
            _model = new Model();
            _view.ButtonLoadFileClickEvent += ButtonLoadFileClick;
            _view.ButtonEditClickEvent += ButtonEditClick;
        }

        private void ButtonLoadFileClick(object Sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog()
            {
                Filter = "All Files(*.*) |*.*| Text Files(*.txt) |*.txt||",
                FilterIndex = 2
            };

            if (open.ShowDialog() == DialogResult.OK)
            {

                Task.Factory.StartNew(() =>
                {

                    using (StreamReader reader = File.OpenText(open.FileName))
                    {
                        _model.Text = reader.ReadToEnd();
                        _view.TextBoxReadOnlyText.Text = _model.Text;
                    }
                });

                _view.ButtonEditFile.Enabled = true;
            }
        }

        private void ButtonEditClick(object sender, EventArgs e)
        {
            var formEdit = new FormEdit();
            new PresenterFormEdit(formEdit, _model);

            formEdit.ShowDialog();
            _view.TextBoxReadOnlyText.Text = _model.Text;
        }
    }
}