﻿using Lesson3H3.Models;
using Lesson3H3.Views;
using System;
using System.Windows.Forms;

namespace Lesson3H3.Presenters
{
    public class PresenterFormEdit
    {
        private readonly IFormEditView _view;
        private readonly IModel _model;

        public PresenterFormEdit(IFormEditView view, IModel model)
        {
            _view = view;
            _model = model;

            _view.ButtonSaveClickEvent += ButtonSaveClick;
            _view.ButtonCancelClickEvent += ButtonCancelClick;
            _view.FormLoadEvent += FormLoad;
        }

        private void ButtonSaveClick(object sender, EventArgs e)
        {
            _model.Text = _view.TextBoxEdit.Text;
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            ((Form)_view).Close();
        }

        private void FormLoad(object sender, EventArgs e)
        {
            _view.TextBoxEdit.Text = _model.Text;
        }
    }
}