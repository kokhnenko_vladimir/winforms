﻿using System;
using System.Windows.Forms;

namespace Lesson3H3.Views
{
    public interface IFormEditView
    {
        Button ButtonSave { get; }
        Button ButtonCancel { get; }
        TextBox TextBoxEdit { get; }

        event EventHandler ButtonSaveClickEvent;
        event EventHandler ButtonCancelClickEvent;
        event EventHandler FormLoadEvent;
    }
}