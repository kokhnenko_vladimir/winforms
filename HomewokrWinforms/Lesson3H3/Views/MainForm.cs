﻿using Lesson3H3.Views;
using System;
using System.Windows.Forms;

namespace Lesson3H3
{
    public partial class MainForm : Form, IMainView
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public Button ButtonLoadFile => buttonLoadFile;

        public Button ButtonEditFile => buttonEdit;

        public TextBox TextBoxReadOnlyText => textBoxReadOnlyText;

        public event EventHandler ButtonLoadFileClickEvent;
        public event EventHandler ButtonEditClickEvent;

        private void ButtonLoadFile_Click(object sender, EventArgs e)
        {
            ButtonLoadFileClickEvent(this, EventArgs.Empty);
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            ButtonEditClickEvent(this, EventArgs.Empty);
        }
    }
}