﻿using System;
using System.Windows.Forms;

namespace Lesson3H3.Views
{
    public interface IMainView
    {
        //event EventHandler ButtonLoadFileEvent;
        //event EventHandler ButtonRedactEvent;

        Button ButtonLoadFile { get; }
        Button ButtonEditFile { get; }
        TextBox TextBoxReadOnlyText { get; }

        event EventHandler ButtonLoadFileClickEvent;
        event EventHandler ButtonEditClickEvent;
    }
}
