﻿using Lesson3H3.Views;
using System;
using System.Windows.Forms;

namespace Lesson3H3
{
    public partial class FormEdit : Form, IFormEditView
    {
        public Button ButtonSave => buttonSave;
        public Button ButtonCancel => buttonCancel;
        public TextBox TextBoxEdit => textBoxEdit;

        public FormEdit()
        {
            InitializeComponent();
        }

        public event EventHandler ButtonSaveClickEvent;
        public event EventHandler ButtonCancelClickEvent;
        public event EventHandler FormLoadEvent;

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            ButtonSaveClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            ButtonCancelClickEvent?.Invoke(this, EventArgs.Empty);
        }

        private void FormEdit_Load(object sender, EventArgs e)
        {
            FormLoadEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}