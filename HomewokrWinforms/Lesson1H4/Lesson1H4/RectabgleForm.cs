﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lesson1H4
{
    public partial class MainRectangleForm : Form
    {
        private static Label DynamicLabel;
        private static int _countStatic;
        Point _top;
        Point _down;

        public MainRectangleForm()
        {
            InitializeComponent();

            _countStatic = 0;
            _top = new Point(0, 0);
            _down = new Point(0, 0);
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _top.X = e.X;
                _top.Y = e.Y;
            }
        }

        private void MainForm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _down.X = e.X;
                _down.Y = e.Y;

                if (_down.X > _top.X && _down.Y > _top.Y)
                {

                }
                else if (_down.X < _top.X && _down.Y < _top.Y)
                {
                    int t = _down.X;
                    _down.X = _top.X;
                    _top.X = t;
                    t = _down.Y;
                    _down.Y = _top.Y;
                    _top.Y = t;
                }
                else if (_down.X < _top.X && _down.Y > _top.Y)
                {
                    int t = _down.X;
                    _down.X = _top.X;
                    _top.X = t;
                }
                else if (_down.Y < _top.Y && _down.X > _top.X)
                {
                    int t = _down.Y;
                    _down.Y = _top.Y;
                    _top.Y = t;
                }

                if ((_down.X - _top.X) < 10 || (_down.Y - _top.Y) < 10)
                {
                    MessageBox.Show($"Минимальный размер статика составляет 10x10");
                }
                else
                {
                    DynamicLabel = new Label
                    {
                        Text = $"(Label № {++_countStatic})",
                        TabIndex = _countStatic,
                        AutoSize = false,
                        BorderStyle = BorderStyle.Fixed3D,
                        Location = _top,
                        Size = new Size(_down.X - _top.X, _down.Y - _top.Y)
                    };
                    Controls.Add(DynamicLabel);

                    DynamicLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(DynamicLabel_MouseClick);
                    DynamicLabel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(DynamicLabel_MouseDoubleClick);
                }
            }
        }

        private void DynamicLabel_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int maxSerialNumber = 0;
                var d = sender as Label;
                int curPosX = e.X + d.Location.X;
                int curPosY = e.Y + d.Location.Y;

                Text = $"Локация = {d.Location.X}, {d.Location.Y}; Координаты мыши {e.X}, {e.Y}";

                List<System.Windows.Forms.Label> listControls = new List<System.Windows.Forms.Label>();

                foreach (var item in Controls)
                {
                    d = item as Label;

                    if (curPosX >= d.Location.X && curPosX <= d.Location.X + d.Size.Width - 1 && curPosY >= d.Location.Y && curPosY <= d.Location.Y + d.Size.Height - 1)
                    {
                        listControls.Add(d);
                        MessageBox.Show($"TabIndex добавляемого в лист элемента {d.TabIndex}");
                        if (d.TabIndex > maxSerialNumber)
                            maxSerialNumber = d.TabIndex;
                    }
                }

                foreach (var item in listControls)
                {
                    if (maxSerialNumber == item.TabIndex)
                        Text = $"{item.Text}; Площадь = {item.Size.Width * item.Size.Height}, Координаты: X = {item.Location.X}, Y = {item.Location.Y}";
                }
            }
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            Text = $"X = {e.X}, Y {e.Y}";
        }

        private void DynamicLabel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int minSerialNumber = int.MaxValue;
            var d = sender as Label;

            int curPosX = e.X + d.Location.X;
            int curPosY = e.Y + d.Location.Y;

            foreach (var item in Controls)
            {
                d = item as Label;

                if (curPosX >= d.Location.X && curPosX <= d.Location.X + d.Size.Width - 1 && curPosY >= d.Location.Y && curPosY <= d.Location.Y + d.Size.Height - 1)
                    if (d.TabIndex < minSerialNumber)
                        minSerialNumber = d.TabIndex;
            }
            if (minSerialNumber != int.MaxValue)
            {
                Controls.RemoveAt(minSerialNumber - 1);

                for (int i = minSerialNumber - 1; i < Controls.Count; i++)
                {
                    Controls[i].TabIndex = Controls[i].TabIndex - 1;
                    Controls[i].Text = $"Label № {Controls[i].TabIndex}";
                }
                _countStatic--;
            }
        }
    }
}
